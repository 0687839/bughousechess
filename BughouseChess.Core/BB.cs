﻿using System.Numerics;
using System.Runtime.CompilerServices;

namespace BughouseChess.Core;

/// <summary>
/// A utility class with methods for creating a manipulating bitboards and indexes.
/// </summary>
public static class BB
{
    /// <summary>
    /// Whether the bit at the provided index is set in the provided bitboard. Bit index starts counting from the least
    /// significant bit.
    /// </summary>
    /// <param name="bb">A bitboard.</param>
    /// <param name="i">Index to check.</param>
    /// <returns>True if the i'th bit is set in bb.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsSet(ulong bb, int i) => (bb >> i & 1) != 0;
    
    /// <summary>
    /// Create a bitboard with the i'th bit set to 1.
    /// </summary>
    /// <param name="i">Index of the 1 bit.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static ulong FromIndex(int i) => 1UL << i;
    
    /// <summary>
    /// Calculate the index of a square (see <see cref="Position"/> for an example) from it's 0-indexed file and rank
    /// number.
    /// </summary>
    /// <param name="file">File number in interval [0,8)</param>
    /// <param name="rank">Rank number in interval [0,8)</param>
    /// <returns>Index of the provided square.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Index(int file, int rank) => file + 8 * rank;
    
    /// <summary>
    /// Get the 0-indexed rank number of the provided index.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Rank(int i) => i switch
    {
        >= 0 and < 8 => 0,
        >= 8 and < 16 => 1,
        >= 16 and < 24 => 2,
        >= 24 and < 32 => 3,
        >= 32 and < 40 => 4,
        >= 40 and < 48 => 5,
        >= 48 and < 56 => 6,
        >= 56 and < 64 => 7,
        _ => -1,
    };
    
    /// <summary>
    /// Get the 0-indexed file number of the provided index.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int File(int i) => i % 8;
    
    /// <summary>
    /// The index of the least significant bit in the provided bitboard.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int LSBIndex(ulong bb) => BitOperations.TrailingZeroCount(bb);
    
    /// <summary>
    /// Whether or not the provided index is in bounds of a bitboard/chessboard.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool InBounds(int i) => i is >= 0 and < 64;
    
    /// <summary>
    /// The amount of bits set to 1 (or population count) in the provided bitboard.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int PopCount(ulong bb) => BitOperations.PopCount(bb);
    
    public static string ToString(ulong bb)
    {
        string o = "";
        for (int row = 7; row >= 0; row--)
        {
            for (int col = 0; col < 8; col++) o += IsSet(bb, Index(col, row)) ? "1 " : ". ";
            o += "\n";
        }

        return o;
    }

    public static readonly ulong Rank7 = 0xFF000000000000;
    public static readonly ulong Rank6 = 0xFF0000000000;
    public static readonly ulong Rank2 = 0xFF00;
    public static readonly ulong Rank3 = 0xFF0000;
    public static readonly ulong NotRank1 = 0xFFFFFFFFFFFFFF00;
    public static readonly ulong Rank1 = 0x00000000000000FF;
    public static readonly ulong NotRank8 = 0xFFFFFFFFFFFFFF;
    public static readonly ulong Rank8 = 0x00000000000000;
}
