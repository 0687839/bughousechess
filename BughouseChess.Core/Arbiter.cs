﻿using System.Runtime.CompilerServices;

namespace BughouseChess.Core;

/// <summary>
/// Arbiter is a basic game management class that starts from a certain <see cref="Position"/>, or the starting position
/// by default. It keeps track of game history, has a <see cref="Searcher"/> and can detect game endings using the
/// <see cref="GameResult"/> method. It automatically updates the searcher's repetition stack to also allow for easy
/// detection of three-fold repetition by the engine. Does not include a clock. Loss by timeout and draw by timeout vs
/// insufficient material must be handled externally.
/// </summary>
public class Arbiter
{
    public enum EndType
    {
        Checkmate,
        Stalemate,
        ThreeFoldRepetition,
        FiftyMoveRule,
        InsufficientMaterial,
    }
    
    Position _pos = Position.Empty();
    Searcher _searcher = new();
    List<(Position, Move)> _hist = new(512);

    public Arbiter(Position? pos = null)
    {
        // Ensure all static classes are initialized.
        RuntimeHelpers.RunClassConstructor(typeof(Zobrist).TypeHandle);
        RuntimeHelpers.RunClassConstructor(typeof(MoveGenerator).TypeHandle);
        RuntimeHelpers.RunClassConstructor(typeof(Score).TypeHandle);
        
        pos ??= Position.StartingPosition();
        _pos.CopyFrom((Position)pos);
        _searcher.AddHistory(_pos.ZobristHash, false);
    }

    /// <summary>
    /// Apply a move to the position and add it to the game history and repetition stack of the searcher.
    /// See underlying function: Position.<see cref="Position.ApplyMove"/>.
    /// </summary>
    /// <param name="move"></param>
    public void ApplyMove(Move move)
    {
        _pos.ApplyMove(move);
        _searcher.AddHistory(_pos.ZobristHash, RepetitionStack.IsIrreversible(move));
        Position posCpy = Position.Empty();
        posCpy.CopyFrom(_pos);
        _hist.Add((posCpy, move));
    }

    /// <summary>
    /// See Searcher.<see cref="Searcher.Think"/>.
    /// </summary>
    public Searcher.Evaluation Think(int? moveTime = null, int? maxDepth = null, int? wtime = null, int? btime = null,
        int? winc = null, int? binc = null) => _searcher.Think(_pos, moveTime, maxDepth, wtime, btime, winc, binc);

    /// <summary>
    /// Cancels a search if one is ongoing.
    /// </summary>
    public void CancelSearch() => _searcher.RequestStop();
    
    /// <summary>
    /// See MoveGenerator.<see cref="MoveGenerator.GenerateAllLegalMoves"/>
    /// </summary>
    public List<Move> LegalMoves() => MoveGenerator.GenerateAllLegalMoves(_pos);

    /// <summary>
    /// See Position.<see cref="Position.To2DArray"/>.
    /// </summary>
    /// <returns></returns>
    public Square[,] PositionTo2DArray() => _pos.To2DArray();

    /// <summary>
    /// See Position.<see cref="Position.IncNumDroppable(Side, PieceType, int)"/>.
    /// </summary>
    public void IncNumDroppable(Side s, PieceType pt, int n = 1) => _pos.IncNumDroppable(s, pt, n);
    
    /// <summary>
    /// See Position.<see cref="Position.SetNumDroppable(Side, PieceType, int)"/>.
    /// </summary>
    public void SetNumDroppable(Side s, PieceType pt, int n) => _pos.SetNumDroppable(s, pt, n);

    /// <summary>
    /// See Position.<see cref="Position.Us"/>.
    /// </summary>
    public Side Us() => _pos.Us();

    /// <summary>
    /// See Position.<see cref="Position.Opp"/>.
    /// </summary>
    public Side Opp() => _pos.Opp();

    /// <summary>
    /// Whether or not the specified side only has a king on the board. Useful for detecting forced draw by insufficient
    /// material.
    /// </summary>
    /// <param name="s">Side to detect for.</param>
    /// <returns>Whether the given side only has a king.</returns>
    public bool HasOnlyKing(Side s) => PieceTypes.NotNoneOrKing.All(pt => _pos.State[(int)s][(int)pt] == 0);

    /// <summary>
    /// Checks if any end condition criteria have been met (see <see cref="EndType"/>)). See which side it applies to
    /// using this.<see cref="Us"/>. For example, if the EndType is Checkmate and Us() returns Side.White, black
    /// checkmates white.
    /// </summary>
    /// <returns>Game's type of end or null if none are met.</returns>
    public EndType? GameResult()
    {
        List<Move> legalMoves = LegalMoves();
        // If there are no legal moves and we are in check, it's checkmate, otherwise it's stalemate.
        if (legalMoves.Count == 0) return MoveGenerator.IsChecked(_pos) ? EndType.Checkmate : EndType.Stalemate;

        int repCount = 0;
        int fiftyMoveRule = 100; // A move is 2 ply (i.e. white moves a piece and then black moves a piece), so 50 * 2.
        foreach (var entry in _hist)
        {
            if (_pos.Equals(entry.Item1)) repCount++;
            if (!entry.Item2.HasFlag(Move.Flag.Capture) && entry.Item2.GetPieceType() != PieceType.Pawn)
                fiftyMoveRule--;
        }
        if (repCount >= 3) return EndType.ThreeFoldRepetition;
        if (fiftyMoveRule <= 0) return EndType.FiftyMoveRule;
        if (HasOnlyKing(_pos.Us()) && HasOnlyKing(_pos.Opp())) return EndType.InsufficientMaterial;
        
        return null;
    }
}