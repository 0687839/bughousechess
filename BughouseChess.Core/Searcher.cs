﻿using System.Diagnostics;

namespace BughouseChess.Core;

public class Searcher
{
    public class Evaluation
    {
        public readonly int Score;
        public readonly Move BestMove;
        public readonly int Depth;
        public readonly Evaluation? Next;

        public Evaluation(int score, Move bestMove, int depth, Evaluation? next)
        {
            Score = score;
            BestMove = bestMove;
            Depth = depth;
            Next = next;
        }
    }
    
    TranspositionTable _tt = new();
    PositionStack _ps = new(200);
    RepetitionStack _rs = new(512);
    int nodesSearched;
    volatile bool Stop;

    public void RequestStop() => Stop = true;
    
    // int StaticExchangeEval(int i, )

    int Quiesce(Position pos, int depth, int ply, int a, int b)
    {
        nodesSearched++;
        
        Span<Move> moves = stackalloc Move[512];
        int mslen;
        
        int staticScore = Score.Static(pos, null, -1);
        if (depth == 0) return staticScore;
        
        bool isChecked = MoveGenerator.IsChecked(pos);
        if (!isChecked)
        {
            // We can use the static evaluation to limit the extent of quiescence search. We can do this by assuming we
            // are not in Zugzwang and applying the null move observation, i.e. most of the time there is a move that
            // will improve our position relative to not making any move at all.
            if (staticScore >= b) return staticScore;
            a = Math.Max(staticScore, a);
            
            mslen = MoveGenerator.GenerateCapturesAndPromotions(pos, moves);
            if (mslen == 0) return Score.Static(pos, null, -1);
        }
        else mslen = MoveGenerator.GenerateAllMoves(pos, moves);
        
        _ps.Push(pos);
        int value = int.MinValue;
        int numChildren = 0;
        for (int i = 0; i < mslen; i++)
        {
            pos.ApplyMove(moves[i]);
            if (!MoveGenerator.IsLegal(pos) || MoveGenerator.IsIllegalDrop(pos, moves[i]))
            {
                _ps.ApplyTop(ref pos);
                continue;
            }
            numChildren++;
            
            value = Math.Max(value, -Quiesce(pos, depth - 1, ply + 1, -b, -a));
            _ps.ApplyTop(ref pos);
            a = Math.Max(a, value);
            if (value >= b)
            {
                _ps.Pop();
                return value;
            }
        }
        _ps.Pop();
        
        // Stalemate can't be detected because if we're not in check, we don't generate all moves and thus we don't
        // perform a full search. Therefore, if there are no children and we're not in check, we just return the static
        // evaluation.
        if (numChildren == 0) return isChecked ? Score.FromMatePly(ply) : staticScore;
        
        // If we haven't evaluated any moves, that means all moves were quiet and therefore not evaluated. We return the
        // static evaluation.
        return value;
    }

    void OrderMoves(Move prevBest, Span<Move> moves, int len)
    {
        for (int i = 0; i < len; i++)
        {
            if (moves[i].Equals(prevBest))
            {
                (moves[0], moves[i]) = (moves[i], moves[0]);
                break;
            }
        }
    }
    
    // Possible optimizations: order child nodes.
    int Eval(Position pos, int depth, int ply, bool irreversible, int a = int.MinValue+1, int b = int.MaxValue)
    {
        nodesSearched++;
        if (_rs.IsRepeated(pos.ZobristHash, ply)) return 0;
        bool ok = _tt.Lookup(pos.ZobristHash, out var te);
        if (ok && te.Depth >= depth)
        {
            if (te.Type == TranspositionTable.Bound.Exact) return te.Value;
            if (te.Type == TranspositionTable.Bound.Lower) a = Math.Max(a, te.Value);
            else if (te.Type == TranspositionTable.Bound.Upper) b = Math.Min(b, te.Value);
            if (a >= b) return te.Value;
        }
        
        Span<Move> moves = stackalloc Move[512];
        int mslen = MoveGenerator.GenerateAllMoves(pos, moves);
        
        if (depth == 0) 
            return Quiesce(pos, 15, ply, a, b);
        
        if (ok && !te.Move.Equals(Move.NullMove)) OrderMoves(te.Move, moves, mslen);
        
        _rs.Push(pos.ZobristHash, irreversible); _ps.Push(pos);
        int numChildren = 0;
        int value = int.MinValue;
        Move bestMove = Move.NullMove;
        TranspositionTable.Bound bound = TranspositionTable.Bound.Upper;
        for (int i = 0; i < mslen && !Stop; i++)
        {
            pos.ApplyMove(moves[i]);
            if (!MoveGenerator.IsLegal(pos) || MoveGenerator.IsIllegalDrop(pos, moves[i]))
            {
                _ps.ApplyTop(ref pos);
                continue;
            }
            
            numChildren++;
            
            value = Math.Max(value, -Eval(pos, depth - 1, ply + 1, RepetitionStack.IsIrreversible(moves[i]), -b, -a));

            _ps.ApplyTop(ref pos);

            if (value > a) // New best (PV) node has been found.
            {
                a = value;
                bestMove = moves[i];
                bound = TranspositionTable.Bound.Exact;
            }
            if (Stop) depth = 1;
            if (value >= b) // Beta cutoff
            {
                _tt.Register(pos.ZobristHash, b, depth, TranspositionTable.Bound.Lower, moves[i]);
                _ps.Pop(); _rs.Pop();
                return value;
            }
        }
        _ps.Pop(); _rs.Pop();
        if (numChildren == 0)
        {
            // If there are no legal moves and current side is in check, it's checkmate. The value of this node will
            // already be set to int.MinValue, because it was never overwritten.
            return MoveGenerator.IsChecked(pos) ? Score.FromMatePly(ply) : 0; // If not in check, it's stalemate.
        }
        
        _tt.Register(pos.ZobristHash, value, depth, bound, bestMove);
        return value;
    }

    public Evaluation Search(Position pos, int maxDepth)
    {
        Stop = false;
        nodesSearched = 0;
        List<Move> moves = MoveGenerator.GenerateAllLegalMoves(pos);
        if (moves.Count == 0) throw new Exception("no legal moves in position");
        long start = Stopwatch.GetTimestamp();
        
        _ps.Push(pos);
        List<TranspositionTable.Entry> pv = new();
        for (int depth = 1; depth <= maxDepth && !Stop; depth++)
        {
            // Ply is repetition stack length minus 1 because the starting position is also recorded in the repetition
            // stack, but doesn't count as a move.
            Eval(pos, depth, _rs.Length() - 1, false);
            _ps.ApplyTop(ref pos);
            pv = TracePV(pos);
            
            if (pv.Count > 0 && Score.IsMate(pv[0].Value))
                Stop = pv[0].Value > 0; // Stop after finding a forced mate for us.
        }

        _ps.Pop(ref pos);
        Evaluation? prev = null;
        for (int i = pv.Count - 1; i >= 0; i--)
            prev = new Evaluation(pv[i].Value, pv[i].Move, pv[i].Depth, prev);
        Stop = false;

        int msElapsed = (int)Stopwatch.GetElapsedTime(start).TotalMilliseconds;
        if (msElapsed != 0) // Apparently this can be 0 sometimes??
            Console.WriteLine($"DEBUG: {nodesSearched} nodes searched {nodesSearched / msElapsed} kn/s");
        return prev!;
    }

    // Resulting position and the move that caused it.
    public void AddHistory(ulong hash, bool irreversible) => _rs.Push(hash, irreversible);

    List<TranspositionTable.Entry> TracePV(Position pos)
    {
        List<TranspositionTable.Entry> pv = new (50);
        _ps.Push(pos);
        while (_tt.Lookup(pos.ZobristHash, out var head) && head.Type != TranspositionTable.Bound.Upper && pv.Count <= 50)
        {
            pv.Add(head);
            pos.ApplyMove(head.Move);
        }
        _ps.Pop(ref pos);
        return pv;
    }

    public Evaluation Think(Position pos, int? moveTime = null, int? maxDepth = null, int? wtime = null, int? btime = null, int? winc = null, int? binc = null)
    {
        maxDepth ??= 40;
        int? time = pos.Us() == Side.White ? wtime : btime;
        int? inc = pos.Us() == Side.White ? winc : binc;
        
        if (moveTime != null && moveTime != 0) time = moveTime; // Fixed time was set.
        else if (time != null && time != 0) // Game has time control, calculate time to use.
        {
            time /= 800; // Estimate that a game will last 40 moves.
            time *= MoveGenerator.GenerateAllLegalMoves(pos).Count; // At 40 legal moves, time = 1/40 of our time.
            if (inc != null) time += inc / 2; // Use half of our increment as well.
        }
        time ??= int.MaxValue; // No fixed move time and no time control, search indefinitely until depth is reached.

        Evaluation? e = null;
        Thread s = new Thread(() => { e = Search(pos, (int)maxDepth); });
        s.Start();
        if (time != int.MaxValue) new Thread(() =>
        {
            Thread.Sleep((int)time);
            RequestStop();
        }).Start();

        s.Join();
        if (e == null) throw new Exception("null evaluation, something unexpected went wrong, this should not happen");
        return e!;
    }
}