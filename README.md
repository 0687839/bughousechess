# Bughouse Chess (INFOB1PICA Group 6)

## Introduction
Welcome to our Bughouse Chess project! Bughouse Chess is a dynamic and strategic team-based variant, where two players collaborate, each playing on a separate board. Captured pieces are passed to teammates, introducing unique and exciting strategic opportunities.

## Features
### Main menu
Our visually appealing main menu provides a seamless user experience with options to:
- Start games
- Configure sound/backgrounds
- Access rules

### Chess engine
Our chess engine boasts a rating around 1550, offering challenging but not impossible gameplay against the computer. A standalone UCI version of the chess engine can be found [here](https://github.com/cfschilham/Alexvis).

### Regular chess and bughouse modes
We offer both regular chess and bughouse chess modes, providing diverse gaming experiences. Regular chess supports local multiplayer and playing against the chess bot. Bughouse chess allows single-player games with AI teammates or local multiplayer with up to four players.

## Installation
1. Clone the repository: `$ git clone git@git.science.uu.nl:0687839/bughousechess.git`
2. Navigate to the `BughouseChess.GUI` project, for example: `$ cd ./BughouseChess/BughouseChess.GUI`. Or open the project in an IDE of your choice.
3. Ensure you have the [SDK of .NET 8.0](https://dotnet.microsoft.com/en-us/download/dotnet/8.0) installed. Build the application via you IDE's interface or using: `$ dotnet build`

## Usage
Launch the game and navigate through the main menu.
Choose between regular chess or bughouse chess modes.
Play against the computer or enjoy a game with friends in local multiplayer.
In bughouse chess, versus mode allows you to play against a human and two bots against each other. Co-op allows you to play against an AI and a teammate to play against another AI.

## Requirements
.NET 8.0

## Contributing
1. Fork the repository.
2. Create a new branch for your feature (git checkout -b feature/YourFeature).
3. Commit your changes (git commit -S -am 'Add new feature').
4. Push to the branch (git push origin feature/YourFeature).
5. Open a Merge Request.

## Acknowledgments
Isja Mannens, our project coach

## Authors
Amir Azagouag  
Arian Maat  
Camiel Schilham  
Ciske Meeuwsen Venema

## Project status
Our project is complete and thoroughly debugged.

## Support
If you have any questions or feedback regarding our project, please feel free to contact us:

Email: a.azagouag@students.uu.nl