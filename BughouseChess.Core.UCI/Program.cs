﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using BughouseChess.Core;

class UCIEngine
{
    static void PrintChessboard(Square[,] board)
    {
        for (int row = 7; row >= 0; row--)
        {
            for (int col = 0; col < 8; col++)
            {
                Console.Write(GetPieceSymbol(board[row, col]) + " ");
            }
            Console.WriteLine();
        }
    }

    static string GetPieceSymbol(Square square)
    {
        switch (square.PieceType)
        {
            case PieceType.None:
                return ".";
            case PieceType.Pawn:
                return square.Side == Side.White ? "P" : "p";
            case PieceType.Knight:
                return square.Side == Side.White ? "N" : "n";
            case PieceType.Bishop:
                return square.Side == Side.White ? "B" : "b";
            case PieceType.Rook:
                return square.Side == Side.White ? "R" : "r";
            case PieceType.Queen:
                return square.Side == Side.White ? "Q" : "q";
            case PieceType.King:
                return square.Side == Side.White ? "K" : "k";
            default:
                return "?";
        }
    }
    static void Main()
    {
        Position currentPosition = Position.StartingPosition(); // Initialize with the starting position

        while (true)
        {
            string input = Console.ReadLine();

            if (input == "uci")
            {
                Console.WriteLine("id name ScaakMotor");
                Console.WriteLine("id author Nicolaas Aegir");
                Console.WriteLine("uciok");
            }
            else if (input == "isready") Console.WriteLine("readyok");
            else if (input == "ucinewgame") currentPosition = Position.StartingPosition();
            else if (input.StartsWith("position")) currentPosition = ParsePosition(input);
            else if (input.StartsWith("go"))
            {
                Move bestMove;
                int evaluation;
                NegaScout.GenerateMove(currentPosition, 5, out evaluation, out bestMove);
                Console.WriteLine($"bestmove {FormatMove(bestMove)}");
                Console.WriteLine($"info score cp {evaluation}");
            }
            else if (input == "quit") break;
            else if (input == "printboard") PrintChessboard(currentPosition.To2DArray());
            else if (input == "eval")
            {
                ConcurrentDictionary<ulong, TranspositionEntry> tt = new();
                List<Move>[] mvAlloc = new List<Move>[6];
                List<Position>[] cldAlloc = new List<Position>[6];
                for (int i = 0; i < 6; i++)
                {
                    mvAlloc[i] = new List<Move>(200);
                    cldAlloc[i] = new List<Position>(200);
                }

                Console.WriteLine($"cp {NegaScout.Eval(currentPosition, 5, mvAlloc, cldAlloc, tt)}");
            }
        }
    }

    static Position ParsePosition(string input)
    {
        string[] parts = input.Split();

        if (parts.Length < 2) return Position.StartingPosition();

        if (parts.Length > 3 && parts[1] == "startpos" && parts[2] == "moves")
        {
            Position p = Position.StartingPosition();
            foreach (var moveString in parts.Skip(3))
            {
                if (moveString == "null")
                {
                    p = p.ApplyMove(Move.NullMove);
                    continue;
                }
                Move move = ParseMove(moveString);
                p = p.ApplyMove(p.GetAllLegalMoves()
                    .First(m => m.GetTo() == move.GetTo() && m.GetFrom() == move.GetFrom() && m.GetPromotion() == move.GetPromotion()));
            }

            return p;
        }
        if (parts[1] == "startpos") return Position.StartingPosition();

        // Invalid input, return the current position
        return Position.StartingPosition();
    }

    public static PieceType ParsePieceType(char c) => c switch
    {
        'q' => PieceType.Queen,
        'r' => PieceType.Rook,
        'b' => PieceType.Bishop,
        'n' => PieceType.Knight,
        _ => PieceType.None,
    };
    
    public static char FormatPieceType(PieceType pt) => pt switch
    {
        PieceType.Queen => 'q',
        PieceType.Rook => 'r',
        PieceType.Bishop => 'b',
        PieceType.Knight => 'n',
        _ => ' ',
    };
    
    public static Move ParseMove(string moveString)
    {
        if (moveString.Length is < 4 or > 5) throw new ArgumentException("Invalid move string length");

        char fromFileChar = moveString[0];
        char fromRankChar = moveString[1];
        char toFileChar = moveString[2];
        char toRankChar = moveString[3];
        char promotion = (char)0;
        if (moveString.Length == 5) promotion = moveString[4];

        if (!char.IsLetter(fromFileChar) || !char.IsDigit(fromRankChar) || !char.IsLetter(toFileChar) || !char.IsDigit(toRankChar))
        {
            throw new ArgumentException("Invalid move string format");
        }

        int fromFile = fromFileChar - 'a';
        int fromRank = fromRankChar - '1';
        int toFile = toFileChar - 'a';
        int toRank = toRankChar - '1';

        int from = Position.GetSquareIndex(fromFile, fromRank);
        int to = Position.GetSquareIndex(toFile, toRank);
        
        return new Move(from, to, PieceType.None, Move.Flag.None, ParsePieceType(promotion));
    }
    
    static string FormatMove(Move move)
    {
        int from = move.GetFrom();
        int to = move.GetTo();
        char fromFile = (char)('a' + Position.GetFile(from) - 1);
        char fromRank = (char)('1' + Position.GetRank(from) - 1);
        char toFile = (char)('a' + Position.GetFile(to) - 1);
        char toRank = (char)('1' + Position.GetRank(to) - 1);

        return $"{fromFile}{fromRank}{toFile}{toRank}" + (move.GetPromotion() != PieceType.None ? FormatPieceType(move.GetPromotion()) : "");
    }
}
