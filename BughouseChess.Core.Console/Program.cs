﻿using BughouseChess.Core;

Arbiter a = new();
// Thread.Sleep(1);
// foreach (var lm in a.LegalMoves())
// {
//     Console.WriteLine(UCI.FromMove(lm));
// }
Thread.Sleep(1);
Searcher.Evaluation e = a.Think(1000);
// if (e == null)
// {
//     Thread.Sleep(1);
//     e = a.Think(2000);
// }

while (e != null)
{
    Console.WriteLine($"{UCI.FromMove(e.BestMove)} {e.Score}");
    e = e.Next;
}
