﻿using BughouseChess.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BughouseChess.GUI
{
    public class BughouseChessGame : ChessGame
    {
        public Chessboard c2;
        public ChessTimer chesstimerWhiteBoard2, chesstimerBlackBoard2;
        public ChessButton resignWhite2, resignBlack2;
        public ChessList blacklist2, whitelist2;
        public Label chessboard2;
        public Label timerWhite2, timerBlack2;
        public Label button3, button4, chesslistwhite2, chesslistblack2;
        public Label turnDotlabel2;

        public BughouseChessGame()
        {
            this.chessboard2 = new Label();
            this.c2 = new Chessboard(chessboard2, (int)(chessboardSize / 2.5), new Point(this.ClientSize.Width / 8 - chessboardSize / 8 - chessboardSize / 16, this.ClientSize.Height / 4 * 3 - chessboardSize / 16), true);

            c1.bughouse = true;

            this.timerWhite2 = new Label();
            this.timerBlack2 = new Label();

            this.button3 = new Label();
            this.button4 = new Label();
            this.chesslistwhite2 = new Label();
            this.chesslistblack2 = new Label();

            this.turnDotlabel2 = new Label();


            button3.BackColor = Color.Transparent;
            button4.BackColor = Color.Transparent;
            timerWhite2.BackColor = Color.Transparent;
            timerBlack2.BackColor = Color.Transparent;
            chesslistwhite2.BackColor = Color.Transparent;
            chesslistblack2.BackColor = Color.Transparent;
            turnDotlabel2.BackColor = Color.Transparent;


            System.Windows.Forms.Timer timerWhiteBoard2 = new System.Windows.Forms.Timer();
            System.Windows.Forms.Timer timerBlackBoard2 = new System.Windows.Forms.Timer();

            timerWhiteBoard2.Interval = 1000;
            timerBlackBoard2.Interval = 1000;

            timerWhiteBoard2.Tag = $"{minute}:0{second}";
            timerBlackBoard2.Tag = $"{minute}:0{second}";


            this.chesstimerWhiteBoard2 = new ChessTimer(new Point(c1.location.X + chessboardSize + chessboardSize / 8 - buttonWidth, c1.location.Y - buttonHeight * 3), new Size(buttonWidth, buttonHeight), $"{timerWhiteBoard2.Tag}", timerWhite2, timerWhiteBoard2);
            this.chesstimerBlackBoard2 = new ChessTimer(new Point(c1.location.X + chessboardSize + chessboardSize / 8 - buttonWidth, c1.location.Y + chessboardSize + chessboardSize / 8 + buttonHeight * 2), new Size(buttonWidth, buttonHeight), $"{timerBlackBoard2.Tag}", timerBlack2, timerBlackBoard2);

            this.resignWhite2 = new ChessButton(new Point(c1.location.X, c1.location.Y - buttonHeight * 3), new Size(buttonWidth, buttonHeight), "Resign", button3);
            this.resignBlack2 = new ChessButton(new Point(c1.location.X, c1.location.Y + chessboardSize + chessboardSize / 8 + buttonHeight * 2), new Size(buttonWidth, buttonHeight), "Resign", button4);

            this.blacklist2 = new ChessList(new Point(c1.location.X + chessboardSize / 16, c1.location.Y + chessboardSize + chessboardSize / 16 + 50), new Size(chessboardSize, 60), c2.piecelistBlack.piecelist, chesslistblack2);
            this.whitelist2 = new ChessList(new Point(c1.location.X + chessboardSize / 16, c1.location.Y - 80), new Size(chessboardSize, 60), c2.piecelistWhite.piecelist, chesslistwhite2);



            c2.timer1 = chesstimerWhiteBoard2;
            c2.timer2 = chesstimerBlackBoard2;
            c2.resignwhite = resignWhite2;
            c2.resignblack = resignBlack2;
            c2.piecelistWhite = whitelist2;
            c2.piecelistBlack = blacklist2;

            c2.turnDot = turnDotlabel2;
            blacklist2.side = Side.Black;
            whitelist2.side = Side.White;

            c1.otherboard = c2;
            c2.otherboard = c1;

            c2.firstSide = Side.Black;


            c2.timer1.form = this;
            c2.timer2.form = this;

            c2.CreateChessboard();


            this.Controls.Add(chessboard2);
            this.Controls.Add(button3);
            this.Controls.Add(button4);
            this.Controls.Add(timerWhite2);
            this.Controls.Add(timerBlack2);
            this.Controls.Add(chesslistwhite2);
            this.Controls.Add(chesslistblack2);
            this.Controls.Add(turnDotlabel2);


            c2.chessboard.MouseClick += c2.Move;
            c2.resignwhite.label.MouseClick += c2.Resign;
            c2.resignblack.label.MouseClick += c2.Resign;
            c2.piecelistBlack.label.MouseClick += (o, mea) => c2.MovefromList(o, mea, c2.piecelistBlack);
            c2.piecelistWhite.label.MouseClick += (o, mea) => c2.MovefromList(o, mea, c2.piecelistWhite);
        }
    }
}
