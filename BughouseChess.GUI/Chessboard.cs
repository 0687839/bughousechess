﻿using System.ComponentModel;
using System.Media;
using System.Net.Sockets;
using BughouseChess.Core;
using static System.Windows.Forms.AxHost;

namespace BughouseChess.GUI
{
    
    public class Chessboard : Form
    {
        public Dictionary<string, Bitmap> pieces = new(){
            {"WhitePawn", Properties.Resources.WhitePawn},
            {"WhiteKnight", Properties.Resources.WhiteKnight},
            {"WhiteBishop", Properties.Resources.WhiteBishop},
            {"WhiteRook", Properties.Resources.WhiteRook},
            {"WhiteQueen", Properties.Resources.WhiteQueen},
            {"WhiteKing", Properties.Resources.WhiteKing},
            {"BlackPawn", Properties.Resources.BlackPawn},
            {"BlackKnight", Properties.Resources.BlackKnight},
            {"BlackBishop", Properties.Resources.BlackBishop},
            {"BlackRook", Properties.Resources.BlackRook},
            {"BlackQueen", Properties.Resources.BlackQueen},
            {"BlackKing", Properties.Resources.BlackKing},
            {"Empty", Properties.Resources.Empty}
        };
        // Everything needed to construct a chessboard and its functionalities, apart from the inherited possesions.
        internal Form form;

        internal int chessboardSize;
        private (int, int) selectedField = (-1, -1);
        internal string ip;
        internal bool active;
        internal bool isHost;
        private bool timerstart = false;
        internal bool playsound;
        private int turn = 1;
        internal Point location;
        internal Label chessboard, turnDot;
        internal Bitmap board, turnDotBitmap;
        internal Chessboard otherboard;
        internal ChessTimer timer1, timer2;
        internal ChessButton resignwhite, resignblack;
        internal ChessList piecelistWhite, piecelistBlack;
        internal byte[] buffer = new byte[4];

        internal Square clicked;
        internal Square selectedSquare;
        internal int selectedlist = -1;
        internal Side firstSide;
        internal bool bughouse = false;
        internal bool bot = false;
        internal bool singleplayer = false;
        internal bool online = false;
        internal bool Host = true;

        Arbiter arbiter = new Arbiter(Position.StartingPosition());

        Button chooseQueen;
        Button chooseRook;
        Button chooseBishop;
        Button chooseKnight;

        Form promotionScreen;

        int from, to;
        PieceType pieceType;
        Move.Flag flag;

        internal Socket sock;
        internal BackgroundWorker MessageReceiver = new BackgroundWorker();
        internal TcpListener server = null;
        internal TcpClient client;

        PieceType promotion = PieceType.Rook;




        public Chessboard()
        {
            this.chessboard = new Label();
            this.chessboardSize = 480;
            this.Location = new Point();
            this.board = new Bitmap(1, 1);
            this.piecelistWhite = new ChessList();
            this.piecelistBlack = new ChessList();
        }
        public Chessboard(Label chessboard, int chessboardSize, Point location, bool bughouse)
        {
            this.bughouse = bughouse;
            this.chessboard = chessboard;
            this.chessboardSize = chessboardSize;
            this.location = location;
            this.board = new Bitmap(chessboardSize, chessboardSize);
            this.piecelistWhite = new ChessList();
            this.piecelistBlack = new ChessList();
            selectedSquare.PieceType = PieceType.None;
        }



        /// <summary>
        /// Creates a chessboard and turn-dot on the screen as well as starting the timer.
        /// </summary>
        /// 
        public void CreateChessboard()
        {
            chessboard.Image = board;
            chessboard.BackColor = Color.Silver;
            chessboard.Size = new Size(chessboardSize + chessboardSize / 8, chessboardSize + chessboardSize / 8);
            chessboard.Location = location;
            chessboard.Paint += Paint;
            turnDot.Image = new Bitmap(chessboardSize / 8, chessboardSize / 8);
            turnDot.BackColor = Color.Transparent;
            turnDot.Size = new Size(chessboardSize / 8, chessboardSize / 8);
            turnDot.Location = new Point(location.X + chessboardSize + chessboardSize / 4, location.Y + chessboardSize / 2);
            turnDot.Paint += Paint;

            if (!timerstart)
            {
                timer1.timer.Tag = $"{timer1.minute}:0{timer1.second}";
                timer1.text = $"{timer1.timer.Tag}";
                timer1.timer.Start();
                timer2.timer.Start();
                timer2.timer.Stop();
                timer1.timer.Tick += timer1.clock;
                timer2.timer.Tick += timer2.clock;
                timerstart = true;
                if (bughouse)
                {
                    timer1.timer.Stop();
                    otherboard.timer1.timer.Start();
                }
            }
        }



        /// <summary>
        /// Instructions for the player waiting on a move.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MessageReceiver_DoWork(object? sender, DoWorkEventArgs e)
        {
            active = false;
            ReceiveMove();
            chessboard.Invalidate();
            active = true;
        }



        /// <summary>
        /// Receives a move and applies it to the board.
        /// </summary>
        private void ReceiveMove()
        {
            Move move;
            byte[] buffer = new byte[5];
            sock.Receive(buffer);
            move = new Move(buffer[0], buffer[1], (PieceType)buffer[2], (Core.Move.Flag)buffer[3], (PieceType)buffer[4]);
            arbiter.ApplyMove(move);
            Timer();
        }



        /// <summary>
        /// Creates a buffer to send over connection
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        public static byte[] BufferCreation(Move move)
        {
            byte[] buffer = new byte[5];
            buffer[0] = (byte)move.GetFrom();
            buffer[1] = (byte)move.GetTo();
            buffer[2] = (byte)move.GetPieceType();
            buffer[3] = (byte)move.GetFlags();
            buffer[4] = (byte)move.GetPromotion();
            return buffer;
        }



        /// <summary>
        /// Painting the chessboard and turn-dot
        /// </summary>
        /// <param name="o"></param>
        /// <param name="pea"></param>
        new void Paint(object o, PaintEventArgs pea)
        {
            SolidBrush brush = new SolidBrush(Color.White);
            Graphics gr = pea.Graphics;
            int fieldSize = chessboardSize / 8;



            if (o == chessboard)
            {
                Square[,] squares = arbiter.PositionTo2DArray();
                string pieceName;


                //Switches the selectedField if firstside is black.
                if (firstSide != Side.White)
                {
                    selectedField.Item1 = 7 - selectedField.Item1;
                    selectedField.Item2 = 7 - selectedField.Item2;
                }

                //Colors the chessboard squares and the pieces on them.
                for (int x = 0; x < 8; x++)
                {
                    for (int y = 0; y < 8; y++)
                    {
                        if ((x + y) % 2 == 0)
                            brush = new SolidBrush(Color.LightGray);
                        if ((x + y) % 2 == 1)
                            brush = new SolidBrush(Color.Gray);
                        gr.FillRectangle(brush, fieldSize * x + chessboardSize / 16, fieldSize * y + chessboardSize / 16, fieldSize, fieldSize);
                        if (selectedField == (x, y))
                            gr.FillEllipse(new SolidBrush(ControlPaint.Dark(brush.Color, 20)), fieldSize * x + chessboardSize / 16, fieldSize * y + chessboardSize / 16, fieldSize, fieldSize);

                        if (firstSide == Side.White)
                        {
                            if (squares[x, 7 - y].PieceType == PieceType.None) continue;
                            pieceName = squares[x, 7 - y].Side + squares[x, 7 - y].PieceType.ToString();
                        }
                        else
                        {
                            if (squares[7 - x, y].PieceType == PieceType.None) continue;
                            pieceName = squares[7 - x, y].Side + squares[7 - x, y].PieceType.ToString();
                        }

                        gr.DrawImage(pieces[pieceName], x * fieldSize + fieldSize / 2, y * fieldSize + fieldSize / 2, fieldSize, fieldSize);
                    }
                }

                if (selectedField != (-1, -1) || selectedlist != -1)
                {
                    //Draws all possible moves from a selected field.
                    if (firstSide == Side.White)
                    {
                        if (!BB.InBounds(BB.Index(selectedField.Item1, 7 - selectedField.Item2))) return;
                        foreach (var move in arbiter.LegalMoves())
                        {
                            if (move.GetFrom() == BB.Index(selectedField.Item1, 7 - selectedField.Item2))
                                gr.FillEllipse(new SolidBrush(ControlPaint.Dark(brush.Color, 20)),
                                    fieldSize * BB.File(move.GetTo()) + chessboardSize / 16 + fieldSize / 4,
                                    fieldSize * (7 - BB.Rank(move.GetTo())) + chessboardSize / 16 + fieldSize / 4, fieldSize / 2,
                                    fieldSize / 2);
                        }
                        //Draws all possible moves from the selected piece from list.
                        if (selectedlist != -1)
                        {
                            if (arbiter.Us() == Side.White)
                            {
                                foreach (var move in arbiter.LegalMoves())
                                {
                                    if (move.HasFlag(Core.Move.Flag.Drop) && move.GetPieceType() == piecelistWhite.piecelist[selectedlist].Item1.PieceType)
                                        gr.FillEllipse(new SolidBrush(ControlPaint.Dark(brush.Color, 20)),
                                        fieldSize * BB.File(move.GetTo()) + chessboardSize / 16 + fieldSize / 4,
                                        fieldSize * (7 - BB.Rank(move.GetTo())) + chessboardSize / 16 + fieldSize / 4, fieldSize / 2,
                                        fieldSize / 2);
                                }
                            }
                            else
                            {
                                foreach (var move in arbiter.LegalMoves())
                                {
                                    if (move.HasFlag(Core.Move.Flag.Drop) && move.GetPieceType() == piecelistBlack.piecelist[selectedlist].Item1.PieceType)
                                        gr.FillEllipse(new SolidBrush(ControlPaint.Dark(brush.Color, 20)),
                                        fieldSize * BB.File(move.GetTo()) + chessboardSize / 16 + fieldSize / 4,
                                        fieldSize * (7 - BB.Rank(move.GetTo())) + chessboardSize / 16 + fieldSize / 4, fieldSize / 2,
                                        fieldSize / 2);
                                }
                            }
                        }
                    }
                    //Draws all possible moves from a selected field (alternative side).
                    else
                    {
                        if (!BB.InBounds(BB.Index(selectedField.Item1, 7 - selectedField.Item2))) return;
                        selectedField.Item1 = 7 - selectedField.Item1;
                        selectedField.Item2 = 7 - selectedField.Item2;
                        foreach (var move in arbiter.LegalMoves())
                        {
                            if (move.GetFrom() == BB.Index(selectedField.Item1, 7 - selectedField.Item2))
                                gr.FillEllipse(new SolidBrush(ControlPaint.Dark(brush.Color, 20)),
                                fieldSize * (7 - (BB.File(move.GetTo()))) + chessboardSize / 16 + fieldSize / 4,
                                fieldSize * (BB.Rank(move.GetTo())) + chessboardSize / 16 + fieldSize / 4, fieldSize / 2,
                                fieldSize / 2);
                        }
                        //Draws all possible moves from the selected piece from list (alternative side).
                        if (selectedlist != -1)
                        {
                            if (arbiter.Us() == Side.White)
                            {
                                foreach (var move in arbiter.LegalMoves())
                                {
                                    if (move.HasFlag(Core.Move.Flag.Drop) && move.GetPieceType() == piecelistWhite.piecelist[selectedlist].Item1.PieceType)
                                        gr.FillEllipse(new SolidBrush(ControlPaint.Dark(brush.Color, 20)),
                                        fieldSize * (7 - BB.File(move.GetTo())) + chessboardSize / 16 + fieldSize / 4,
                                        fieldSize * BB.Rank(move.GetTo()) + chessboardSize / 16 + fieldSize / 4, fieldSize / 2,
                                        fieldSize / 2);
                                }
                            }
                            else
                            {
                                foreach (var move in arbiter.LegalMoves())
                                {
                                    if (move.HasFlag(Core.Move.Flag.Drop) && move.GetPieceType() == piecelistBlack.piecelist[selectedlist].Item1.PieceType)
                                        gr.FillEllipse(new SolidBrush(ControlPaint.Dark(brush.Color, 20)),
                                        fieldSize * (7 - BB.File(move.GetTo())) + chessboardSize / 16 + fieldSize / 4,
                                        fieldSize * BB.Rank(move.GetTo()) + chessboardSize / 16 + fieldSize / 4, fieldSize / 2,
                                        fieldSize / 2);
                                }
                            }
                        }
                    }
                }
            }

            //Draws the dot to indicate the current side that can play.
            else if (o == turnDot)
            {
                if (arbiter.Us() == Side.White)
                    brush = new SolidBrush(Color.White);
                else
                    brush = new SolidBrush(Color.Black);
                gr.FillEllipse(brush, 0, 0, chessboardSize / 8, chessboardSize / 8);
            }
        }



        /// <summary>
        /// Moves the pieces on the board and updates timers
        /// </summary>
        /// <param name="o"></param>
        /// <param name="mea"></param>
        public void Move(object o, MouseEventArgs mea)
        {
            //Checks if board is active
            if (!active)
                return;

            int x = (mea.X - chessboardSize / 16) / (chessboardSize / 8);
            int y = (mea.Y - chessboardSize / 16) / (chessboardSize / 8);


            //Switches the board if firstside is black.
            if (firstSide != Side.White)
            {
                x = 7 - x;
                y = 7 - y;
            }
            //Check if the click was on the board
            if (!BB.InBounds(BB.Index(x, 7 - y)))
            {
                selectedField = (-1, -1);
                return;
            }


            clicked = arbiter.PositionTo2DArray()[x, 7 - y];

            //Checks if the same piece has been selected.
            if (selectedField == (x, y))
            {
                selectedField = (-1, -1);
                chessboard.Refresh();
                return;
            }


            List<Move> moves = arbiter.LegalMoves();

            //Moves a piece from the list to the board
            if (selectedlist != -1)
            {
                Move move = new Move(BB.Index(x, 7 - y), BB.Index(x, 7 - y), selectedSquare.PieceType, Core.Move.Flag.Drop, selectedSquare.PieceType);

                //Checks if drop move is legal
                if (moves.Contains(move))
                {
                    if (arbiter.Us() == Side.White)
                    {
                        piecelistWhite.piecelist.RemoveAt(selectedlist);
                        piecelistWhite.label.Refresh();
                    }
                    else
                    {
                        piecelistBlack.piecelist.RemoveAt(selectedlist);
                        piecelistBlack.label.Refresh();
                    }
                    arbiter.ApplyMove(move);
                    selectedlist = -1;
                    TurnEndEvents();
                }
                //Deselects the piece from list if selected move was not valid.
                else
                {
                    if (arbiter.Us() == Side.White)
                    {
                        piecelistWhite.piecelist[selectedlist] = (piecelistWhite.piecelist[selectedlist].Item1, false);
                        piecelistWhite.label.Refresh();
                    }
                    else
                    {
                        piecelistBlack.piecelist[selectedlist] = (piecelistBlack.piecelist[selectedlist].Item1, false);
                        piecelistBlack.label.Refresh();
                    }
                }
                selectedSquare.PieceType = PieceType.None; selectedSquare.Side = Side.White;
                selectedlist = -1;
                chessboard.Refresh();
                return;
            }


            //Deselects field if same piece has been clicked on.
            if (clicked.Side == arbiter.Us() && clicked.PieceType != PieceType.None)
            {
                selectedField = (x, y);
                chessboard.Refresh();
                return;
            }

            //Returns if no legal field was selected.
            if (selectedField == (-1, -1)) return;

            
            //Checks if selected move is legal.
            foreach (var move in moves)
            {
                if (move.GetFrom() != BB.Index(selectedField.Item1, 7 - selectedField.Item2))
                    continue;
                if (move.GetTo() != BB.Index(x, 7 - y)) continue;

                //Creates form for choosing promotion piece.
                if ((BB.Rank(move.GetTo()) == 7 || BB.Rank(move.GetTo()) == 0) && move.GetPieceType() == PieceType.Pawn)
                {

                    from = move.GetFrom();
                    to = move.GetTo();
                    pieceType = move.GetPieceType();
                    flag = move.GetFlags();
                    promotionScreen = new Form(); promotionScreen.BackColor = Color.White; promotionScreen.StartPosition = FormStartPosition.CenterScreen; promotionScreen.Size = new Size(140, 350);
                    chooseQueen = new Button(); chooseQueen.Location = new Point(12, 12); chooseQueen.Size = new Size(100, 60); chooseQueen.Text = "Queen"; chooseQueen.TextAlign = ContentAlignment.MiddleCenter; chooseQueen.BackColor = Color.Silver; promotionScreen.Controls.Add(chooseQueen); chooseQueen.Click += PromotionClick;
                    chooseRook = new Button(); chooseRook.Location = new Point(12, 84); chooseRook.Size = new Size(100, 60); chooseRook.Text = "Rook"; chooseRook.TextAlign = ContentAlignment.MiddleCenter; chooseRook.BackColor = Color.Silver; promotionScreen.Controls.Add(chooseRook); chooseRook.Click += PromotionClick;
                    chooseBishop = new Button(); chooseBishop.Location = new Point(12, 156); chooseBishop.Size = new Size(100, 60); chooseBishop.Text = "Bishop"; chooseBishop.TextAlign = ContentAlignment.MiddleCenter; chooseBishop.BackColor = Color.Silver; promotionScreen.Controls.Add(chooseBishop); chooseBishop.Click += PromotionClick;
                    chooseKnight = new Button(); chooseKnight.Location = new Point(12, 228); chooseKnight.Size = new Size(100, 60); chooseKnight.Text = "Knight"; chooseKnight.TextAlign = ContentAlignment.MiddleCenter; chooseKnight.BackColor = Color.Silver; promotionScreen.Controls.Add(chooseKnight); chooseKnight.Click += PromotionClick;
                    promotionScreen.Visible = true;
                    return;

                }
                //Applies normal move if no promotion
                else
                {
                    arbiter.ApplyMove(move);
                    //Sends the move over to the other player if online.
                    if (online)
                    {
                        buffer = BufferCreation(move);
                        sock.Send(buffer);
                        MessageReceiver.RunWorkerAsync();
                    }
                    if (clicked.PieceType != PieceType.None || move.HasFlag(Core.Move.Flag.EnPassant))
                        AddCapturedPiece(this, clicked);
                    TurnEndEvents();
                    return;
                }
            }
            //Move was not legal.
            selectedField = (-1, -1);
            chessboard.Refresh();
        }

        /// <summary>
        /// Move Piece from list to chessboard
        /// </summary>
        /// <param name="o"></param>
        /// <param name="mea"></param>
        /// <param name="chesslist"></param>
        public void MovefromList(object o, MouseEventArgs mea, ChessList chesslist)
        {
            int x = (mea.X - chesslist.s.Height / 2) / chesslist.s.Height;

            //Checks if item has been selected.
            if (x < chesslist.piecelist.Count && chesslist.side == arbiter.Us())
            {
                selectedField = (-1, -1);
                //Deselects piece from list if the same piece has been selected.
                if (x == selectedlist || selectedlist != -1)
                {
                    chesslist.piecelist[selectedlist] = (chesslist.piecelist[selectedlist].Item1, false);
                    chesslist.label.Refresh();
                    selectedlist = -1;
                    chessboard.Refresh();
                    return;
                }

                //Selects the square.
                selectedSquare.PieceType = chesslist.piecelist[x].Item1.PieceType;
                selectedSquare.Side = chesslist.piecelist[x].Item1.Side;
                selectedlist = x;
                chesslist.piecelist[x] = (chesslist.piecelist[x].Item1, true);
                chesslist.label.Refresh();
                chessboard.Refresh();
            }
        }



        /// <summary>
        /// Plays sound when moving a piece.
        /// How to use a soundplayer from: https://learn.microsoft.com/en-us/dotnet/desktop/winforms/controls/how-to-play-a-sound-from-a-windows-form?view=netframeworkdesktop-4.8.
        /// Move sound from: https://www.youtube.com/watch?v=7skwR49UhqA.
        /// </summary>
        public void PlaySound()
        {
            if (playsound == true)
            {
                SoundPlayer sound = new SoundPlayer(Properties.Resources.Move);
                sound.Play();
            }
        }



        /// <summary>
        /// Eventhandler for promotionform click
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        void PromotionClick(object o, EventArgs e)
        {
            if (o == chooseQueen)
                promotion = PieceType.Queen;
            if (o == chooseRook)
                promotion = PieceType.Rook;
            if (o == chooseBishop)
                promotion = PieceType.Bishop;
            if (o == chooseKnight)
                promotion = PieceType.Knight;

            //Applies the promotion move.
            Move promotionmove = new Move(from, to, pieceType, flag, promotion);
            if (clicked.PieceType != PieceType.None)
                AddCapturedPiece(this, clicked);
            TurnEndEvents();
            arbiter.ApplyMove(promotionmove);
            chessboard.Invalidate();
            turnDot.Invalidate();
            //Sends the promotion move to other player if online.
            if (online)
            {
                buffer = BufferCreation(promotionmove);
                sock.Send(buffer);
                MessageReceiver.RunWorkerAsync();
            }
            promotionScreen.Close();
        }

        /// <summary>
        /// Adds pieces from the board to teammate's list.
        /// </summary>
        /// <param name="board"></param>
        /// <param name="captured"></param>
        public void AddCapturedPiece(Chessboard board, Square captured)
        {
            if (!bughouse) return;
            
            //Checks if captured piece should be added
                if (singleplayer)
                {
                    if (captured.Side == Side.Black)
                        board.otherboard.piecelistBlack.piecelist.Add((captured, false));
                    else
                    {
                        captured.Side = Side.White;
                        if (captured.PieceType == PieceType.None) captured.PieceType = PieceType.Pawn;
                        board.otherboard.piecelistWhite.piecelist.Add((captured, false));
                    }
                }
                else if (clicked.Side != Side.Both)
                {
                    if (clicked.Side == Side.Black)
                        board.otherboard.piecelistBlack.piecelist.Add((captured, false));
                    if (clicked.Side == Side.White)
                        board.otherboard.piecelistWhite.piecelist.Add((captured, false));
                }
                else
                {
                    if (arbiter.Us() == Side.Black)
                        board.otherboard.piecelistBlack.piecelist.Add((captured, false));
                    if (arbiter.Us() == Side.White)
                        board.otherboard.piecelistWhite.piecelist.Add((captured, false));
                }
                board.otherboard.arbiter.IncNumDroppable(captured.Side, captured.PieceType, 1);

                chessboard.Refresh();
                otherboard.chessboard.Refresh();
                board.otherboard.piecelistBlack.label.Refresh();
                board.otherboard.piecelistWhite.label.Refresh();
                board.piecelistBlack.label.Refresh();
                board.piecelistWhite.label.Refresh();
        }


        public void BotCapturePieceEvent(Searcher.Evaluation e, Square[,] state, Chessboard board)
        {
            //Adds captured piece if best move is a capturing move.
            if (e.BestMove.HasFlag(Core.Move.Flag.Capture) || e.BestMove.HasFlag(Core.Move.Flag.EnPassant))
            {
                Square captured;
                if (e.BestMove.HasFlag(Core.Move.Flag.EnPassant))
                    captured.PieceType = PieceType.Pawn;
                else
                    captured.PieceType = state[BB.File(e.BestMove.GetTo()), BB.Rank(e.BestMove.GetTo())].PieceType;
                captured.Side = state[BB.File(e.BestMove.GetTo()), BB.Rank(e.BestMove.GetTo())].Side;
                if (e.BestMove.HasFlag(Core.Move.Flag.EnPassant))
                    captured.Side = state[BB.File(e.BestMove.GetTo() - Position.PawnPush(board.arbiter.Us())), BB.Rank(e.BestMove.GetTo())  - Position.PawnPush(board.arbiter.Us())].Side;
                AddCapturedPiece(board.otherboard, captured);
            }

            //Removes piece from list if best move is a drop move.
            if (e.BestMove.HasFlag(Core.Move.Flag.Drop))
            {
                if (board.otherboard.arbiter.Us() == Side.White)
                {
                    board.otherboard.piecelistWhite.piecelist.Remove((new Square(e.BestMove.GetPieceType(), board.otherboard.arbiter.Us()), false));
                    board.otherboard.piecelistWhite.label.Refresh();
                }
                else
                {
                    board.otherboard.piecelistBlack.piecelist.Remove((new Square(e.BestMove.GetPieceType(), board.otherboard.arbiter.Us()), false));
                    board.otherboard.piecelistBlack.label.Refresh();
                }
                board.otherboard.selectedlist = -1;
            }
        }



        /// <summary>
        /// Botmoves for different chess gamemodes.
        /// </summary>
        public void BotMove()
        {
            if (bughouse && !singleplayer)
            {
                if (bot && otherboard.bot && !singleplayer)
                    GameChange(this, otherboard);

                //Searches best move in current state.
                Searcher.Evaluation e = otherboard.arbiter.Think(2000);
                Square[,] state = otherboard.arbiter.PositionTo2DArray();

                BotCapturePieceEvent(e, state, this);
                otherboard.arbiter.ApplyMove(e.BestMove);
                piecelistBlack.label.Refresh();
                piecelistWhite.label.Refresh();
                otherboard.piecelistBlack.label.Refresh();
                otherboard.piecelistWhite.label.Refresh();
                otherboard.chessboard.Refresh();
                otherboard.turnDot.Refresh();
                Timer();
            }

            //Bot functionality for singleplayer chess.
            else
            {
                //Singpleayer bughouse chess bots.
                if (bughouse)
                {
                    //Black bot on player board.
                    if (turn == -1 && otherboard.turn == 1)
                    {
                        Searcher.Evaluation oe = arbiter.Think(2000);
                        Square[,] state = arbiter.PositionTo2DArray();
                        BotCapturePieceEvent(oe, state, otherboard);
                        arbiter.ApplyMove(oe.BestMove);
                        Timer();
                        chessboard.Refresh();
                        turnDot.Refresh();
                    }
                    //White bot on otherboard.
                    else if (turn == -1 && otherboard.turn == -1)
                    {
                        Searcher.Evaluation oe = otherboard.arbiter.Think(2000);
                        Square[,] state = arbiter.PositionTo2DArray();
                        BotCapturePieceEvent(oe, state, this);
                        otherboard.arbiter.ApplyMove(oe.BestMove);
                        Timer();
                        otherboard.chessboard.Refresh();
                        otherboard.turnDot.Refresh();
                    }
                    //Black bot on otherboard.
                    else if (turn == 1 && otherboard.turn == -1)
                    {
                        Searcher.Evaluation oe = otherboard.arbiter.Think(2000);
                        Square[,] state = arbiter.PositionTo2DArray();
                        BotCapturePieceEvent(oe, state, this);
                        otherboard.arbiter.ApplyMove(oe.BestMove);
                        Timer();
                        otherboard.chessboard.Refresh();
                        otherboard.turnDot.Refresh();
                    }
                    //Player turn.
                    else
                    {
                        PlaySound();
                        return;
                    }


                    PlaySound();
                    BotMove();
                }
                //Singleplayer normal chess bot.
                else
                {
                    Searcher.Evaluation e = arbiter.Think(1500);
                    arbiter.ApplyMove(e.BestMove);
                    Timer();
                    chessboard.Refresh();
                    turnDot.Refresh();
                }
            }
            PlaySound();
        }

        /// <summary>
        /// Changes the positions and sizes of the chessboards
        /// </summary>
        /// <param name="largeBoard"></param>
        /// <param name="smallBoard"></param>
        public void GameChange(Chessboard largeBoard, Chessboard smallBoard)
        {
            //Save points for switching.
            Point lp = largeBoard.chessboard.Location;
            Point sp = smallBoard.chessboard.Location;
            Point lp2 = largeBoard.location;
            Point sp2 = smallBoard.location;
            Size ls = largeBoard.chessboard.Size;
            Size ss = smallBoard.chessboard.Size;
            Point tls1 = largeBoard.timer1.label.Location;
            Point tls2 = largeBoard.timer2.label.Location;
            Point lls1 = largeBoard.piecelistBlack.label.Location;
            Point lls2 = largeBoard.piecelistWhite.label.Location;

            int i = lp2.X + largeBoard.chessboardSize / 2 + largeBoard.chessboardSize / 16;


            //Switches right or left depending on the current positions.
            if (sp.X > lp.X)
            {
                int x = i - (sp.X + smallBoard.chessboardSize + smallBoard.chessboardSize / 8 - i);
                sp.X = x;
                largeBoard.chessboard.Location = sp;
                largeBoard.location = sp2;
            }
            else
            {
                int x = i + (i - (smallBoard.Location.X + (smallBoard.chessboardSize + smallBoard.chessboardSize / 8)));
                sp.X = x - largeBoard.chessboardSize / 8;
                largeBoard.chessboard.Location = sp;
                largeBoard.location = sp2;
            }

            //Switches the board locations.
            largeBoard.chessboard.Size = ss;
            smallBoard.chessboard.Location = lp;
            smallBoard.chessboard.Size = ls;
            smallBoard.location = lp2;
            smallBoard.chessboardSize = 480;
            largeBoard.chessboardSize = (int)(chessboardSize / 2.5);
            largeBoard.timer1.label.Location = new Point(-1000, -1000);
            largeBoard.timer2.label.Location = new Point(-1000, -1000);
            largeBoard.piecelistBlack.label.Location = new Point(-1000, -1000);
            largeBoard.piecelistWhite.label.Location = new Point(-1000, -1000);
            smallBoard.timer1.label.Location = tls2;
            smallBoard.timer2.label.Location = tls1;
            smallBoard.piecelistBlack.label.Location = lls2;
            smallBoard.piecelistWhite.label.Location = lls1;
            largeBoard.turnDot.Location = new Point(sp.X + largeBoard.chessboardSize + largeBoard.chessboardSize / 4, sp.Y + largeBoard.chessboardSize / 2);
            smallBoard.turnDot.Location = new Point(lp.X + smallBoard.chessboardSize + smallBoard.chessboardSize / 4, lp.Y + smallBoard.chessboardSize / 2);
            largeBoard.turnDot.Size = new Size(largeBoard.chessboardSize / 8, largeBoard.chessboardSize / 8);
            smallBoard.turnDot.Size = new Size(smallBoard.chessboardSize / 8, smallBoard.chessboardSize / 8);
            smallBoard.piecelistBlack.label.Refresh();
            smallBoard.piecelistWhite.label.Refresh();
            largeBoard.piecelistBlack.label.Refresh();
            largeBoard.piecelistWhite.label.Refresh();
            smallBoard.chessboard.Refresh();
            largeBoard.chessboard.Refresh();
            smallBoard.turnDot.Refresh();
            largeBoard.turnDot.Refresh();
            largeBoard.active = false;
            smallBoard.active = true;
            Thread.Sleep(100);
        }

        /// <summary>
        /// Starts the next timer in the specified gamemode
        /// </summary>
        public void Timer()
        {
            //Timer for singleplayer bughouse chess.
            if (bughouse && singleplayer)
            {
                if (timerstart && (turn == 1 && otherboard.turn == 1))
                {
                    timer1.timer.Stop();
                    turn *= -1;
                    otherboard.timer1.timer.Start();
                }
                else if (timerstart && (turn == -1 && otherboard.turn == 1))
                {
                    otherboard.timer1.timer.Stop();
                    otherboard.turn *= -1;
                    otherboard.timer2.timer.Start();
                }
                else if (timerstart && (turn == -1 && otherboard.turn == -1))
                {
                    timer2.timer.Stop();
                    turn *= -1;
                    otherboard.timer2.timer.Start();
                }
                else if (timerstart && (turn == 1 && otherboard.turn == -1))
                {
                    otherboard.timer2.timer.Stop();
                    otherboard.turn *= -1;
                    otherboard.timer1.timer.Start();
                }
            }
            //Timer for Co-op bughouse chess.
            else if (bughouse && (bot && otherboard.bot))
            {
                if (timerstart && (turn == 1 && otherboard.turn == 1))
                {
                    timer1.timer.Stop();
                    turn *= -1;
                    otherboard.timer1.timer.Start();
                }
                else if (timerstart && (turn == -1 && otherboard.turn == 1))
                {
                    otherboard.timer1.timer.Stop();
                    otherboard.turn *= -1;
                    otherboard.timer2.timer.Start();
                }
                else if (timerstart && (turn == -1 && otherboard.turn == -1))
                {
                    timer2.timer.Stop();
                    turn *= -1;
                    otherboard.timer2.timer.Start();
                }
                else if (timerstart && (turn == 1 && otherboard.turn == -1))
                {
                    otherboard.timer2.timer.Stop();
                    otherboard.turn *= -1;
                    otherboard.timer1.timer.Start();
                }
            }
            //Timer for 4 player bughouse chess.
            else if (bughouse && !otherboard.bot)
            {
                if (timerstart && (turn == 1 && otherboard.turn == 1))
                {
                    timer1.timer.Stop();
                    turn *= -1;
                    otherboard.timer1.timer.Start();
                }
                else if (timerstart && (turn == 1 && otherboard.turn == -1))
                {
                    timer1.timer.Stop();
                    turn *= -1;
                    otherboard.timer2.timer.Start();
                }
                else if (timerstart && (turn == -1 && otherboard.turn == -1))
                {
                    timer2.timer.Stop();
                    turn *= -1;
                    otherboard.timer2.timer.Start();
                }
                else if (timerstart && (turn == -1 && otherboard.turn == 1))
                {
                    timer2.timer.Stop();
                    turn *= -1;
                    otherboard.timer1.timer.Start();
                }
            }
            //Timer for versus bughouse chess.
            else if (bughouse && otherboard.bot)
            {
                if (timerstart && turn == 1)
                {
                    timer1.timer.Stop();
                    turn *= -1;
                    timerstart = false;
                }
                else if (timerstart && turn == -1)
                {
                    timer2.timer.Stop();
                    turn *= -1;
                    timerstart = false;
                }
                else if (turn == -1)
                {
                    timer2.timer.Start();
                    timerstart = true;
                }
                else
                {
                    timer1.timer.Start();
                    timerstart = true;
                }
            }
            //Timer for normal chess.
            else
            {
                if (timerstart && turn == 1)
                {
                    timer1.timer.Stop();
                    turn *= -1;
                    timer2.timer.Start();
                }
                else if (timerstart && turn == -1)
                {
                    timer1.timer.Start();
                    turn *= -1;
                    timer2.timer.Stop();
                }
            }
        }



        /// <summary>
        /// Events at the end of a turn.
        /// </summary>
        public void TurnEndEvents()
        {
            PlaySound();
            selectedField = (-1, -1);
            chessboard.Refresh();
            turnDot.Refresh();
            if (arbiter.GameResult() != null)
            {
                if (arbiter.GameResult() == Arbiter.EndType.Checkmate)
                {
                    if (bughouse)
                    {
                        if (arbiter.Us() == Side.White)
                            MessageBox.Show($" Checkmate, Team2 won!", "END", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else
                            MessageBox.Show($" Checkmate, Team1 won!", "END", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                        MessageBox.Show($" Checkmate, {arbiter.Opp().ToString()} won!", "END", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show($" {arbiter.GameResult().ToString()}, Draw", "END", MessageBoxButtons.OK, MessageBoxIcon.Information);
                timer1.timer.Stop();
                timer2.timer.Stop();
                if (bughouse)
                {
                    otherboard.timer1.timer.Stop();
                    otherboard.timer2.timer.Stop();
                }
                form.Close();
            }
            Timer();
            if (bughouse && !(otherboard.bot || bot))
                GameChange(this, otherboard);
            if (bot)
                BotMove();
            else if (bughouse && otherboard.bot)
                BotMove();
        }



        /// <summary>
        /// Contains the instructions for what to do when any player resignes.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        public void Resign(object obj, EventArgs e)
        {
            DialogResult answer = MessageBox.Show(" Are you certain you want to resign?", "Resign?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (answer == DialogResult.Yes)
            {
                timer1.timer.Stop();
                timer2.timer.Stop();
                if (bughouse)
                {
                    otherboard.timer1.timer.Stop();
                    otherboard.timer2.timer.Stop();
                    if(arbiter.Us() == Side.White)
                        MessageBox.Show($"Game over, Team1 lost.", "END", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show($"Game over, Team2 lost.", "END", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show($"Game over, {arbiter.Us()} lost.", "END", MessageBoxButtons.OK, MessageBoxIcon.Information);
                form.Close();
            }
        }
    }
}
