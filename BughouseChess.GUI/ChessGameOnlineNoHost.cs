﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;

namespace BughouseChess.GUI
{
    internal class ChessGameOnlineNoHost : ChessGame
    {
        internal ChessGameOnlineNoHost(string ip)
        {
            c1.online = true;
            c1.Host = false;
            c1.MessageReceiver.DoWork += c1.MessageReceiver_DoWork;
            CheckForIllegalCrossThreadCalls = false;
            c1.active = false;
            c1.ip = ip;
            try
            {
                c1.client = new TcpClient(c1.ip, 5732);
                c1.sock = c1.client.Client;
                c1.MessageReceiver.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }


        }
    }
}
