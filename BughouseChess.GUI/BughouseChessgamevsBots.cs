﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BughouseChess.GUI
{
    internal class BughouseChessGamevsBots : BughouseChessGame
    {
        internal BughouseChessGamevsBots()
        {
            c1.bot = true;
            c2.bot = true;
            c1.singleplayer = true;
        }
    }
}
