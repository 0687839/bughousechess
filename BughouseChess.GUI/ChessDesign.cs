﻿namespace BughouseChess.GUI
{
    public class ChessDesign : Form
    {
        public Point p;
        public Size s;
        public Label label;
        public Bitmap bitmap;
        public Brush brush = Brushes.LightGray;
        int edge = 4;

        public ChessDesign()
        {
            this.p = new Point(0, 0);
            this.s = new Size(0, 0);
            this.label = new Label();
            this.bitmap = new Bitmap(1, 1);
            label.Image = bitmap;
            label.Paint += Draw;
        }

        /// <summary>
        /// Draws Graphical User Interfaces in our own design.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="pea"></param>
        public virtual void Draw(object o, PaintEventArgs pea)
        {
            Graphics gr = pea.Graphics;
            gr.FillRectangle(Brushes.Black, s.Height / 2, 0, s.Width - s.Height, s.Height);
            gr.FillEllipse(Brushes.Black, 0, 0, s.Height, s.Height);
            gr.FillEllipse(Brushes.Black, s.Width - s.Height, 0, s.Height, s.Height);

            gr.FillRectangle(brush, s.Height / 2, edge, s.Width - s.Height, s.Height - 2 * edge);
            gr.FillEllipse(brush, edge, edge, s.Height - 2 * edge, s.Height - 2 * edge);
            gr.FillEllipse(brush, s.Width - s.Height + edge, edge, s.Height - 2 * edge, s.Height - 2 * edge);
        }
    }
}
