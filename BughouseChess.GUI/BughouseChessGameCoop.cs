﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BughouseChess.GUI
{
    internal class BughouseChessGameCoop : BughouseChessGame
    {
        internal BughouseChessGameCoop()
        {
            c1.bot = true;
            c2.bot = true;
        }
    }
}
