﻿using BughouseChess.Core;

namespace BughouseChess.GUI
{
    public class ChessList : ChessDesign
    {
        public Dictionary<string, Bitmap> pieces = new(){
            {"WhitePawn", Properties.Resources.WhitePawn},
            {"WhiteKnight", Properties.Resources.WhiteKnight},
            {"WhiteBishop", Properties.Resources.WhiteBishop},
            {"WhiteRook", Properties.Resources.WhiteRook},
            {"WhiteQueen", Properties.Resources.WhiteQueen},
            {"WhiteKing", Properties.Resources.WhiteKing},
            {"BlackPawn", Properties.Resources.BlackPawn},
            {"BlackKnight", Properties.Resources.BlackKnight},
            {"BlackBishop", Properties.Resources.BlackBishop},
            {"BlackRook", Properties.Resources.BlackRook},
            {"BlackQueen", Properties.Resources.BlackQueen},
            {"BlackKing", Properties.Resources.BlackKing},
            {"Empty", Properties.Resources.Empty}
        };
        public List<(Square, bool)> piecelist;
        public Side side;
        public ChessList()
        {
            this.p = new Point(0, 0);
            this.s = new Size(0, 0);
            this.piecelist = new List<(Square, bool)>();
            this.label = new Label();
            this.bitmap = new Bitmap(1, 1);
            label.Image = bitmap;
            label.Paint += Draw;
        }

        public ChessList(Point p, Size s, List<(Square, bool)> piecelist, Label label)
        {
            this.p = p;
            this.s = s;
            this.piecelist = piecelist;
            this.label = label;
            this.bitmap = new Bitmap(s.Width, s.Height);
            label.Size = s;
            label.Location = p;
            label.Image = bitmap;
            label.Paint += Draw;
        }

        public override void Draw(Object o, PaintEventArgs pea)
        {
            Graphics gr = pea.Graphics;
            base.Draw(o, pea);
            for (int i = 0; i < piecelist.Count; i++)
            {
                if (i == 6)
                {
                    gr.DrawString($"+{piecelist.Count - 6}", new Font("Arial", 24), Brushes.Black, new Point(i * s.Height + 40, 10));
                    break;
                }
                if (piecelist[i].Item2)
                    gr.FillEllipse(new SolidBrush(ControlPaint.Dark(Color.White, 20)), i * s.Height + s.Height / 2, 0, s.Height, s.Height);
                string pieceName = piecelist[i].Item1.Side + piecelist[i].Item1.PieceType.ToString();
                gr.DrawImage(pieces[pieceName], i * s.Height + s.Height / 2, 0, s.Height, s.Height);
            }
        }
    }
}
