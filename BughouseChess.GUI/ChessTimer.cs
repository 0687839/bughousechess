﻿namespace BughouseChess.GUI
{
    /// <summary>
    /// Custom made timer.
    /// </summary>
    public class ChessTimer : ChessDesign
    {
        public Form form;
        public string text;
        protected int edge = 4;
        protected int change = 1;
        public System.Windows.Forms.Timer timer;
        public int minute = 10;
        public int second = 0;

        public ChessTimer()
        {
            this.text = "Default";
            this.timer = new System.Windows.Forms.Timer();
            label.Paint += Draw;
        }

        public ChessTimer(Point p, Size s, string text, Label label, System.Windows.Forms.Timer timer)
        {
            this.p = p;
            this.s = s;
            this.text = text;
            this.label = label;
            this.timer = timer;
            this.bitmap = new Bitmap(s.Width, s.Height);
            label.Size = new Size(s.Width, s.Height);
            label.Location = p;
            label.Image = bitmap;
            label.Paint += Draw;
        }

        public override void Draw(object o, PaintEventArgs pea)
        {
            Graphics gr = pea.Graphics;

            base.Draw(o, pea);

            Font font = new Font("Times New Roman", 24, FontStyle.Bold);
            SizeF stringsize = gr.MeasureString(text, font, s.Width);
            gr.DrawString(text, font, Brushes.Black, new Point((s.Width - (int)stringsize.Width) / 2, (s.Height - (int)stringsize.Height) / 2));
        }

        //Timer functions and methods from https://learn.microsoft.com/en-us/dotnet/api/system.windows.forms.timer.tick?view=windowsdesktop-8.0#system-windows-forms-timer-tick and https://learn.microsoft.com/en-us/dotnet/api/system.windows.forms.timer?view=windowsdesktop-8.0.

        /// <summary>
        /// Makes sure the timer counts down from any given time to 0:00.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        public void clock(object o, EventArgs e)
        {
            if (second == 0)
            {
                second = 59;
                minute--;
                timer.Tag = $"{minute}:{second}";
            }
            else if (second == 10 || second < 10)
            {
                second--;
                timer.Tag = $"{minute}:0{second}";
            }
            else if (second > 10 && second <= 59)
            {
                second--;
                timer.Tag = $"{minute}:{second}";
            }
            this.text = $"{timer.Tag}";
            if (minute == 0 && second == 0)
            {
                timer.Stop();
                MessageBox.Show(" You have lost the game \n You used up all of your time", "Time is up", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                form.Close();
            }
            label.Invalidate();
        }
    }
}
