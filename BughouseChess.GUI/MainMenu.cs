﻿using System.Media;

namespace BughouseChess.GUI
{
    /// <summary>
    /// Contains everything for building a main menu. 
    /// Got help from: https://www.youtube.com/watch?v=iy4JC5yA3B4.
    /// </summary>
    public partial class MainMenu : Form
    {
        public string setbackground = "Standard";
        bool soundplay = true;
        public MainMenu()
        {
            InitializeComponent();
        }

        Dictionary<string, Bitmap> backgrounds = new(){
            {"Standard", Properties.Resources.StandardBackground},
            {"Winter", Properties.Resources.WinterBackground},
            {"Summer", Properties.Resources.SummerBackground},
            {"Easter", Properties.Resources.EasterBackground},
            {"Christmas", Properties.Resources.ChristmasBackground},
            {"Sinterklaas", Properties.Resources.SinterklaasBackground}
        };


        private void MainMenu_Load(object sender, EventArgs e)
        {
            Games_Panel.Hide();
            Options_Panel.Hide();
            BackgroundsPanel.Hide();
            Rules_Panel.Hide();
            Information_Panel.Hide();
            Single_Panel.Hide();
            Multiplayer_Panel.Hide();
            Local_Panel.Hide();
            LocalPlayers_Panel.Hide();
            LocalTeam_Panel.Hide();
            Host_Panel.Hide();
            IP_Panel.Hide();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        /// <summary>
        /// Makes sure the colour of the button changes while hovering.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseHover(object sender, EventArgs e)
        {
            // Buttons on the main panel.
            if (sender == Start) { Start.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Rules) { Rules.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Options) { Options.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Information) { Information.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Quit) { Quit.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the options panel.
            if (sender == BackGround) { BackGround.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Back1) { Back1.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == SoundOn) { SoundOn.BackColor = Color.DimGray; }
            if (sender == SoundOff) { SoundOff.BackColor = Color.DimGray; }

            // Button on the backgrounds panel.
            if (sender == GoOptions) { GoOptions.Image = Properties.Resources.button_main_menu_silver_click; }

            // Button on the games panel.
            if (sender == Singleplayer) { Singleplayer.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Multiplayer) { Multiplayer.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Back2) { Back2.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the rules panel.
            if (sender == Back4) { Back4.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the information panel.
            if (sender == Back3) { Back3.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the singleplayer panel.
            if (sender == SingleChess) { SingleChess.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == SingleBughouse) { SingleBughouse.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == GoGames1) { GoGames1.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the multiplayer panel.
            if (sender == Local) { Local.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == LAN) { LAN.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == GoGames2) { GoGames2.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the local panel.
            if (sender == LocalChess) { LocalChess.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == LocalBughouse) { LocalBughouse.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == LocalBack) { LocalBack.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the local number of players panel.
            if (sender == TwoPlayers) { TwoPlayers.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == FourPlayers) { FourPlayers.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == LocalPlayersBack) { LocalPlayersBack.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the local teams panel.
            if (sender == Local_Team) { Local_Team.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == Local_Versus) { Local_Versus.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == LocalTeamBack) { LocalTeamBack.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the host panel.
            if (sender == HostButton) { HostButton.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == NoHostButton) { NoHostButton.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == HostBack) { HostBack.Image = Properties.Resources.button_main_menu_silver_click; }

            // Buttons on the IP panel.
            if (sender == GoIP) { GoIP.Image = Properties.Resources.button_main_menu_silver_click; }
            if (sender == IPBack) { IPBack.Image = Properties.Resources.button_main_menu_silver_click; }

            // Pictures on the backgrounds panel.
            if (sender == Standard) { Standard.BackColor = Color.DimGray; }
            if (sender == Winter) { Winter.BackColor = Color.DimGray; }
            if (sender == Summer) { Summer.BackColor = Color.DimGray; }
            if (sender == Easter) { Easter.BackColor = Color.DimGray; }
            if (sender == Christmas) { Christmas.BackColor = Color.DimGray; }
            if (sender == Sinterklaas) { Sinterklaas.BackColor = Color.DimGray; }
            Cursor = Cursors.Hand;
        }

        /// <summary>
        /// Makes sure the colour of the button changes back when stop hovering.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseLeave(object sender, EventArgs e)
        {
            // Buttons on the main panel.
            if (sender == Start) { Start.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Rules) { Rules.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Options) { Options.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Information) { Information.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Quit) { Quit.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the options panel.
            if (sender == BackGround) { BackGround.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Back1) { Back1.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == SoundOn) { SoundOn.BackColor = Color.White; }
            if (sender == SoundOff) { SoundOff.BackColor = Color.White; }

            // Button on the backgrounds panel.
            if (sender == GoOptions) { GoOptions.Image = Properties.Resources.button_main_menu_silver; }

            // Button on the games panel.
            if (sender == Singleplayer) { Singleplayer.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Multiplayer) { Multiplayer.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Back2) { Back2.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the rules panel.
            if (sender == Back4) { Back4.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the information panel.
            if (sender == Back3) { Back3.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the singleplayer panel.
            if (sender == SingleChess) { SingleChess.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == SingleBughouse) { SingleBughouse.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == GoGames1) { GoGames1.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the multiplayer panel.
            if (sender == Local) { Local.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == LAN) { LAN.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == GoGames2) { GoGames2.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the local panel.
            if (sender == LocalChess) { LocalChess.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == LocalBughouse) { LocalBughouse.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == LocalBack) { LocalBack.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the local number of players panel.
            if (sender == TwoPlayers) { TwoPlayers.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == FourPlayers) { FourPlayers.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == LocalPlayersBack) { LocalPlayersBack.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the local teams panel.
            if (sender == Local_Team) { Local_Team.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == Local_Versus) { Local_Versus.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == LocalTeamBack) { LocalTeamBack.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the host panel.
            if (sender == HostButton) { HostButton.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == NoHostButton) { NoHostButton.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == HostBack) { HostBack.Image = Properties.Resources.button_main_menu_silver; }

            // Buttons on the IP panel.
            if (sender == GoIP) { GoIP.Image = Properties.Resources.button_main_menu_silver; }
            if (sender == IPBack) { IPBack.Image = Properties.Resources.button_main_menu_silver; }

            // Pictures on the backgrounds panel.
            if (sender == Standard) { Standard.BackColor = Color.Gray; }
            if (sender == Winter) { Winter.BackColor = Color.Gray; }
            if (sender == Summer) { Summer.BackColor = Color.Gray; }
            if (sender == Easter) { Easter.BackColor = Color.Gray; }
            if (sender == Christmas) { Christmas.BackColor = Color.Gray; }
            if (sender == Sinterklaas) { Sinterklaas.BackColor = Color.Gray; }
            Cursor = Cursors.Default;
        }

        /// <summary>
        /// Contain all events for every GUI you can click on.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Click(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            // GUI on the main panel.
            if (sender == Start)
            {
                Main_Panel.Hide();
                Games_Panel.Show();
            }

            if (sender == Rules)
            {
                Main_Panel.Hide();
                Rules_Panel.Show();
            }

            if (sender == Options)
            {
                Main_Panel.Hide();
                Options_Panel.Show();
            }

            if (sender == Information)
            {
                Main_Panel.Hide();
                Information_Panel.Show();
            }

            if (sender == Quit)
            {
                Application.Exit();
            }

            // GUI for going back to main menu.
            if (sender == Back1 || sender == Back2 || sender == Back3 || sender == Back4)
            {
                Games_Panel.Hide();
                Options_Panel.Hide();
                Rules_Panel.Hide();
                Information_Panel.Hide();
                Main_Panel.Show();
            }

            // GUI on the games panel.
            if (sender == Singleplayer)
            {
                Games_Panel.Hide();
                Single_Panel.Show();
            }

            if (sender == Multiplayer)
            {
                Games_Panel.Hide();
                Multiplayer_Panel.Show();
            }

            // GUI on the singleplayer panel.
            if (sender == SingleChess)
            {
                ChessGame Game = new ChessGamevsBot();

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;
                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            if (sender == SingleBughouse)
            {
                BughouseChessGame Game = new BughouseChessGamevsBots();

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;
                Game.c2.playsound = soundplay;
                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            // GUI on the multiplayer panel.
            if (sender == Local)
            {
                Multiplayer_Panel.Hide();
                Local_Panel.Show();
            }

            if (sender == LAN)
            {
                Multiplayer_Panel.Hide();
                Host_Panel.Show();
            }

            // GUI on the local panel.
            if (sender == LocalChess)
            {
                ChessGame Game = new ChessGame();

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;
                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            if (sender == LocalBughouse)
            {
                Local_Panel.Hide();
                LocalPlayers_Panel.Show();
            }

            // GUI on the local players panel.
            if (sender == TwoPlayers)
            {
                LocalPlayers_Panel.Hide();
                LocalTeam_Panel.Show();
            }

            if (sender == FourPlayers)
            {
                BughouseChessGame Game = new BughouseChessGame();

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;
                Game.c2.playsound = soundplay;

                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            if (sender == LocalPlayersBack)
            {
                LocalPlayers_Panel.Hide();
                Local_Panel.Show();
            }

            // GUI on the local teams panel.
            if (sender == Local_Team)
            {
                BughouseChessGame Game = new BughouseChessGameCoop();

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;
                Game.c2.playsound = soundplay;
                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            if (sender == Local_Versus)
            {
                BughouseChessGame Game = new BughouseChessGameVersus();

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;
                Game.c2.playsound = soundplay;
                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            if (sender == LocalTeamBack)
            {
                LocalTeam_Panel.Hide();
                LocalPlayers_Panel.Show();
            }

            // GUI for going back to the multiplayer panel.
            if (sender == LocalBack || sender == HostBack)
            {
                Local_Panel.Hide();
                Host_Panel.Hide();
                Multiplayer_Panel.Show();
            }

            // GUI for going back to the games panel.
            if (sender == GoGames1 || sender == GoGames2)
            {
                Single_Panel.Hide();
                Multiplayer_Panel.Hide();
                Games_Panel.Show();
            }

            // GUI on the host panel.
            if (sender == HostButton)
            {
                ChessGame Game = new ChessGameOnlineHost();

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;

                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            if (sender == NoHostButton)
            {
                Host_Panel.Hide();
                IP_Panel.Show();
            }

            // GUI on the IP panel.
            if (sender == GoIP)
            {
                ChessGame Game = new ChessGameOnlineNoHost($"{IPadressFill.Text}");

                Game.BackgroundImage = background();
                Game.c1.playsound = soundplay;

                Visible = false;
                if (!Game.IsDisposed)
                    Game.ShowDialog();
                Visible = true;
            }

            if (sender == IPBack)
            {
                IP_Panel.Hide();
                Host_Panel.Show();
            }

            // GUI on the options panel.
            if (sender == BackGround)
            {
                Options_Panel.Hide();
                BackgroundsPanel.Show();
            }

            if (sender == SoundOn || sender == SoundOff)
            {
                if (sender == SoundOn)
                {
                    soundplay = true;
                }

                if (sender == SoundOff)
                {
                    soundplay = false;
                }
            }

            // GUI on the backgrounds panel.
            if (sender == Standard || sender == Winter || sender == Summer || sender == Easter || sender == Christmas || sender == Sinterklaas)
            {
                if (sender == Standard) { setbackground = "Standard"; }
                if (sender == Winter) { setbackground = "Winter"; }
                if (sender == Summer) { setbackground = "Summer"; }
                if (sender == Easter) { setbackground = "Easter"; }
                if (sender == Christmas) { setbackground = "Christmas"; }
                if (sender == Sinterklaas) { setbackground = "Sinterklaas"; }
                MessageBox.Show(" Your chosen background has been set.", "Background", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (sender == GoOptions)
            {
                BackgroundsPanel.Hide();
                Options_Panel.Show();
            }

            if (soundplay)
            {
                SoundPlayer sound = new SoundPlayer(Properties.Resources.Move);
                sound.Play();
            }
        }

        /// <summary>
        /// Sets the background.
        /// </summary>
        /// <returns></returns>
        Bitmap background()
        {
            Bitmap bitmap = new Bitmap(ClientSize.Width, ClientSize.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.DrawImage(backgrounds[setbackground], 0, 0, ClientSize.Width, ClientSize.Height);
            return bitmap;
        }
        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        /// <summary>
        ///     Background images from:
        ///     https://nl.pinterest.com/pin/725290714980930020/.
        ///     https://www.vecteezy.com/photo/29169573-wooden-christmas-background.
        ///     https://pixabay.com/photos/wood-board-structure-boards-grain-591631/.
        ///     https://www.bongo.nl/sinterklaascadeaus/.
        ///     https://pngtree.com/freebackground/wintry-texture-snowflakes-and-ice-adorn-frozen-wooden-board_13583600.html.
        ///     https://in.pinterest.com/pin/hd-wallpaper-sand-beach-background-board-star-shell-summer-wood-marine--595319644507071576/.
        /// </summary>
    }
}
