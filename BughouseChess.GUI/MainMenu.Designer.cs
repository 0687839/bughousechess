﻿namespace BughouseChess.GUI
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            Main_Panel = new Panel();
            Information = new Button();
            Rules = new Button();
            Quit = new Button();
            Options = new Button();
            Start = new Button();
            Title = new Label();
            Options_Panel = new Panel();
            SoundOff = new Button();
            SoundOn = new Button();
            Sounds = new Label();
            Back1 = new Button();
            BackGround = new Button();
            OptionsTitle = new Label();
            Games_Panel = new Panel();
            Multiplayer = new Button();
            Back2 = new Button();
            Singleplayer = new Button();
            Games = new Label();
            BackgroundsPanel = new Panel();
            L_Sinterklaas = new Label();
            L_Christmas = new Label();
            L_Easter = new Label();
            L_Summer = new Label();
            L_Winter = new Label();
            L_Standard = new Label();
            Sinterklaas = new PictureBox();
            Christmas = new PictureBox();
            Easter = new PictureBox();
            Summer = new PictureBox();
            Winter = new PictureBox();
            Standard = new PictureBox();
            GoOptions = new Button();
            BackgroundTitle = new Label();
            Information_Panel = new Panel();
            L_Information = new Label();
            Back3 = new Button();
            Information_Title = new Label();
            Rules_Panel = new Panel();
            L_Rules = new Label();
            Back4 = new Button();
            RulesTitle = new Label();
            Single_Panel = new Panel();
            SingleBughouse = new Button();
            SingleChess = new Button();
            GoGames1 = new Button();
            Single_PanelTitle = new Label();
            Multiplayer_Panel = new Panel();
            LAN = new Button();
            Local = new Button();
            GoGames2 = new Button();
            Multiplayer_PanelTitle = new Label();
            Local_Panel = new Panel();
            LocalBughouse = new Button();
            LocalChess = new Button();
            LocalBack = new Button();
            Local_PanelTitle = new Label();
            LocalTeam_Panel = new Panel();
            Local_Versus = new Button();
            Local_Team = new Button();
            LocalTeamBack = new Button();
            LocalTeam_PanelTitle = new Label();
            LocalPlayers_Panel = new Panel();
            FourPlayers = new Button();
            TwoPlayers = new Button();
            LocalPlayersBack = new Button();
            LocalPlayers_PanelTitle = new Label();
            Host_Panel = new Panel();
            NoHostButton = new Button();
            HostButton = new Button();
            HostBack = new Button();
            Host_PanelTitle = new Label();
            IP_Panel = new Panel();
            GoIP = new Button();
            IPadressFill = new TextBox();
            IPBack = new Button();
            IP_PanelTitle = new Label();
            Main_Panel.SuspendLayout();
            Options_Panel.SuspendLayout();
            Games_Panel.SuspendLayout();
            BackgroundsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)Sinterklaas).BeginInit();
            ((System.ComponentModel.ISupportInitialize)Christmas).BeginInit();
            ((System.ComponentModel.ISupportInitialize)Easter).BeginInit();
            ((System.ComponentModel.ISupportInitialize)Summer).BeginInit();
            ((System.ComponentModel.ISupportInitialize)Winter).BeginInit();
            ((System.ComponentModel.ISupportInitialize)Standard).BeginInit();
            Information_Panel.SuspendLayout();
            Rules_Panel.SuspendLayout();
            Single_Panel.SuspendLayout();
            Multiplayer_Panel.SuspendLayout();
            Local_Panel.SuspendLayout();
            LocalTeam_Panel.SuspendLayout();
            LocalPlayers_Panel.SuspendLayout();
            Host_Panel.SuspendLayout();
            IP_Panel.SuspendLayout();
            SuspendLayout();
            // 
            // Main_Panel
            // 
            Main_Panel.BackColor = Color.Silver;
            Main_Panel.BorderStyle = BorderStyle.FixedSingle;
            Main_Panel.Controls.Add(Information);
            Main_Panel.Controls.Add(Rules);
            Main_Panel.Controls.Add(Quit);
            Main_Panel.Controls.Add(Options);
            Main_Panel.Controls.Add(Start);
            Main_Panel.Controls.Add(Title);
            Main_Panel.Location = new Point(175, 154);
            Main_Panel.Name = "Main_Panel";
            Main_Panel.Padding = new Padding(10, 0, 0, 0);
            Main_Panel.Size = new Size(900, 592);
            Main_Panel.TabIndex = 0;
            Main_Panel.Paint += panel1_Paint;
            // 
            // Information
            // 
            Information.BackColor = Color.Transparent;
            Information.BackgroundImageLayout = ImageLayout.Center;
            Information.FlatAppearance.BorderSize = 0;
            Information.FlatStyle = FlatStyle.Flat;
            Information.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Information.Image = Properties.Resources.button_main_menu_silver;
            Information.Location = new Point(200, 384);
            Information.Margin = new Padding(0);
            Information.Name = "Information";
            Information.Size = new Size(500, 72);
            Information.TabIndex = 5;
            Information.Text = "Information";
            Information.UseVisualStyleBackColor = false;
            Information.Click += Click;
            Information.MouseLeave += MouseLeave;
            Information.MouseHover += MouseHover;
            // 
            // Rules
            // 
            Rules.BackColor = Color.Transparent;
            Rules.BackgroundImageLayout = ImageLayout.Center;
            Rules.FlatAppearance.BorderSize = 0;
            Rules.FlatStyle = FlatStyle.Flat;
            Rules.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Rules.Image = Properties.Resources.button_main_menu_silver;
            Rules.Location = new Point(200, 192);
            Rules.Margin = new Padding(0);
            Rules.Name = "Rules";
            Rules.Size = new Size(500, 72);
            Rules.TabIndex = 4;
            Rules.Text = "Rules";
            Rules.UseVisualStyleBackColor = false;
            Rules.Click += Click;
            Rules.MouseLeave += MouseLeave;
            Rules.MouseHover += MouseHover;
            // 
            // Quit
            // 
            Quit.BackColor = Color.Transparent;
            Quit.BackgroundImageLayout = ImageLayout.Center;
            Quit.FlatAppearance.BorderSize = 0;
            Quit.FlatStyle = FlatStyle.Flat;
            Quit.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Quit.Image = Properties.Resources.button_main_menu_silver;
            Quit.Location = new Point(200, 480);
            Quit.Margin = new Padding(0);
            Quit.Name = "Quit";
            Quit.Size = new Size(500, 72);
            Quit.TabIndex = 3;
            Quit.Text = "Quit";
            Quit.UseVisualStyleBackColor = false;
            Quit.Click += Click;
            Quit.MouseDown += MouseHover;
            Quit.MouseLeave += MouseLeave;
            Quit.MouseHover += MouseHover;
            Quit.MouseUp += MouseLeave;
            // 
            // Options
            // 
            Options.BackColor = Color.Transparent;
            Options.BackgroundImageLayout = ImageLayout.Center;
            Options.FlatAppearance.BorderSize = 0;
            Options.FlatStyle = FlatStyle.Flat;
            Options.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Options.Image = Properties.Resources.button_main_menu_silver;
            Options.Location = new Point(200, 288);
            Options.Margin = new Padding(0);
            Options.Name = "Options";
            Options.Size = new Size(500, 72);
            Options.TabIndex = 2;
            Options.Text = "Options";
            Options.UseVisualStyleBackColor = false;
            Options.Click += Click;
            Options.MouseDown += MouseHover;
            Options.MouseLeave += MouseLeave;
            Options.MouseHover += MouseHover;
            Options.MouseUp += MouseLeave;
            // 
            // Start
            // 
            Start.BackColor = Color.Transparent;
            Start.BackgroundImageLayout = ImageLayout.Center;
            Start.FlatAppearance.BorderSize = 0;
            Start.FlatStyle = FlatStyle.Flat;
            Start.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Start.Image = Properties.Resources.button_main_menu_silver;
            Start.Location = new Point(200, 96);
            Start.Margin = new Padding(0);
            Start.Name = "Start";
            Start.Size = new Size(500, 72);
            Start.TabIndex = 1;
            Start.Tag = "";
            Start.Text = "Start Game";
            Start.UseVisualStyleBackColor = false;
            Start.Click += Click;
            Start.MouseDown += MouseHover;
            Start.MouseLeave += MouseLeave;
            Start.MouseHover += MouseHover;
            Start.MouseUp += MouseLeave;
            // 
            // Title
            // 
            Title.AutoSize = true;
            Title.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            Title.Location = new Point(230, 5);
            Title.Name = "Title";
            Title.Size = new Size(441, 68);
            Title.TabIndex = 0;
            Title.Text = "Bughouse Chess";
            Title.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Options_Panel
            // 
            Options_Panel.BackColor = Color.Silver;
            Options_Panel.BorderStyle = BorderStyle.FixedSingle;
            Options_Panel.Controls.Add(SoundOff);
            Options_Panel.Controls.Add(SoundOn);
            Options_Panel.Controls.Add(Sounds);
            Options_Panel.Controls.Add(Back1);
            Options_Panel.Controls.Add(BackGround);
            Options_Panel.Controls.Add(OptionsTitle);
            Options_Panel.Location = new Point(175, 250);
            Options_Panel.Name = "Options_Panel";
            Options_Panel.Padding = new Padding(10, 0, 0, 0);
            Options_Panel.Size = new Size(900, 400);
            Options_Panel.TabIndex = 4;
            Options_Panel.Paint += panel1_Paint_1;
            // 
            // SoundOff
            // 
            SoundOff.BackColor = Color.White;
            SoundOff.Font = new Font("Times New Roman", 14.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            SoundOff.Location = new Point(465, 255);
            SoundOff.Name = "SoundOff";
            SoundOff.Size = new Size(143, 30);
            SoundOff.TabIndex = 12;
            SoundOff.Text = "Sounds Off";
            SoundOff.UseVisualStyleBackColor = false;
            SoundOff.Click += Click;
            SoundOff.MouseLeave += MouseLeave;
            SoundOff.MouseHover += MouseHover;
            // 
            // SoundOn
            // 
            SoundOn.BackColor = Color.White;
            SoundOn.Font = new Font("Times New Roman", 14.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            SoundOn.Location = new Point(292, 255);
            SoundOn.Name = "SoundOn";
            SoundOn.Size = new Size(143, 30);
            SoundOn.TabIndex = 11;
            SoundOn.Text = "Sounds On";
            SoundOn.UseVisualStyleBackColor = false;
            SoundOn.Click += Click;
            SoundOn.MouseLeave += MouseLeave;
            SoundOn.MouseHover += MouseHover;
            // 
            // Sounds
            // 
            Sounds.AutoSize = true;
            Sounds.BorderStyle = BorderStyle.FixedSingle;
            Sounds.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Sounds.Location = new Point(384, 192);
            Sounds.Name = "Sounds";
            Sounds.Size = new Size(117, 38);
            Sounds.TabIndex = 7;
            Sounds.Text = "Sounds";
            // 
            // Back1
            // 
            Back1.BackColor = Color.Transparent;
            Back1.BackgroundImageLayout = ImageLayout.Center;
            Back1.FlatAppearance.BorderSize = 0;
            Back1.FlatStyle = FlatStyle.Flat;
            Back1.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Back1.Image = Properties.Resources.button_main_menu_silver;
            Back1.Location = new Point(200, 288);
            Back1.Margin = new Padding(0);
            Back1.Name = "Back1";
            Back1.Size = new Size(500, 72);
            Back1.TabIndex = 3;
            Back1.Text = "Go to Main";
            Back1.UseVisualStyleBackColor = false;
            Back1.Click += Click;
            Back1.MouseLeave += MouseLeave;
            Back1.MouseHover += MouseHover;
            // 
            // BackGround
            // 
            BackGround.BackColor = Color.Transparent;
            BackGround.BackgroundImageLayout = ImageLayout.Center;
            BackGround.FlatAppearance.BorderSize = 0;
            BackGround.FlatStyle = FlatStyle.Flat;
            BackGround.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            BackGround.Image = Properties.Resources.button_main_menu_silver;
            BackGround.Location = new Point(200, 96);
            BackGround.Margin = new Padding(0);
            BackGround.Name = "BackGround";
            BackGround.Size = new Size(500, 72);
            BackGround.TabIndex = 1;
            BackGround.Text = "Backgrounds";
            BackGround.UseVisualStyleBackColor = false;
            BackGround.Click += Click;
            BackGround.MouseLeave += MouseLeave;
            BackGround.MouseHover += MouseHover;
            // 
            // OptionsTitle
            // 
            OptionsTitle.AutoSize = true;
            OptionsTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            OptionsTitle.Location = new Point(335, 5);
            OptionsTitle.Name = "OptionsTitle";
            OptionsTitle.Size = new Size(230, 68);
            OptionsTitle.TabIndex = 0;
            OptionsTitle.Text = "Options";
            OptionsTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Games_Panel
            // 
            Games_Panel.BackColor = Color.Silver;
            Games_Panel.BorderStyle = BorderStyle.FixedSingle;
            Games_Panel.Controls.Add(Multiplayer);
            Games_Panel.Controls.Add(Back2);
            Games_Panel.Controls.Add(Singleplayer);
            Games_Panel.Controls.Add(Games);
            Games_Panel.Location = new Point(175, 250);
            Games_Panel.Name = "Games_Panel";
            Games_Panel.Padding = new Padding(10, 0, 0, 0);
            Games_Panel.Size = new Size(900, 400);
            Games_Panel.TabIndex = 8;
            // 
            // Multiplayer
            // 
            Multiplayer.BackColor = Color.Transparent;
            Multiplayer.BackgroundImageLayout = ImageLayout.Center;
            Multiplayer.FlatAppearance.BorderSize = 0;
            Multiplayer.FlatStyle = FlatStyle.Flat;
            Multiplayer.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Multiplayer.Image = Properties.Resources.button_main_menu_silver;
            Multiplayer.Location = new Point(200, 192);
            Multiplayer.Margin = new Padding(0);
            Multiplayer.Name = "Multiplayer";
            Multiplayer.Size = new Size(500, 72);
            Multiplayer.TabIndex = 4;
            Multiplayer.Text = "Multiplayer";
            Multiplayer.UseVisualStyleBackColor = false;
            Multiplayer.Click += Click;
            Multiplayer.MouseLeave += MouseLeave;
            Multiplayer.MouseHover += MouseHover;
            // 
            // Back2
            // 
            Back2.BackColor = Color.Transparent;
            Back2.BackgroundImageLayout = ImageLayout.Center;
            Back2.FlatAppearance.BorderSize = 0;
            Back2.FlatStyle = FlatStyle.Flat;
            Back2.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Back2.Image = Properties.Resources.button_main_menu_silver;
            Back2.Location = new Point(200, 288);
            Back2.Margin = new Padding(0);
            Back2.Name = "Back2";
            Back2.Size = new Size(500, 72);
            Back2.TabIndex = 3;
            Back2.Text = "Go to Main";
            Back2.UseVisualStyleBackColor = false;
            Back2.Click += Click;
            Back2.MouseLeave += MouseLeave;
            Back2.MouseHover += MouseHover;
            // 
            // Singleplayer
            // 
            Singleplayer.BackColor = Color.Transparent;
            Singleplayer.BackgroundImageLayout = ImageLayout.Center;
            Singleplayer.FlatAppearance.BorderSize = 0;
            Singleplayer.FlatStyle = FlatStyle.Flat;
            Singleplayer.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Singleplayer.Image = Properties.Resources.button_main_menu_silver;
            Singleplayer.Location = new Point(200, 96);
            Singleplayer.Margin = new Padding(0);
            Singleplayer.Name = "Singleplayer";
            Singleplayer.Size = new Size(500, 72);
            Singleplayer.TabIndex = 1;
            Singleplayer.Text = "Singleplayer";
            Singleplayer.UseVisualStyleBackColor = false;
            Singleplayer.Click += Click;
            Singleplayer.MouseLeave += MouseLeave;
            Singleplayer.MouseHover += MouseHover;
            // 
            // Games
            // 
            Games.AutoSize = true;
            Games.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            Games.Location = new Point(345, 5);
            Games.Name = "Games";
            Games.Size = new Size(203, 68);
            Games.TabIndex = 0;
            Games.Text = "Games";
            Games.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // BackgroundsPanel
            // 
            BackgroundsPanel.BackColor = Color.Silver;
            BackgroundsPanel.BorderStyle = BorderStyle.FixedSingle;
            BackgroundsPanel.Controls.Add(L_Sinterklaas);
            BackgroundsPanel.Controls.Add(L_Christmas);
            BackgroundsPanel.Controls.Add(L_Easter);
            BackgroundsPanel.Controls.Add(L_Summer);
            BackgroundsPanel.Controls.Add(L_Winter);
            BackgroundsPanel.Controls.Add(L_Standard);
            BackgroundsPanel.Controls.Add(Sinterklaas);
            BackgroundsPanel.Controls.Add(Christmas);
            BackgroundsPanel.Controls.Add(Easter);
            BackgroundsPanel.Controls.Add(Summer);
            BackgroundsPanel.Controls.Add(Winter);
            BackgroundsPanel.Controls.Add(Standard);
            BackgroundsPanel.Controls.Add(GoOptions);
            BackgroundsPanel.Controls.Add(BackgroundTitle);
            BackgroundsPanel.Location = new Point(175, 10);
            BackgroundsPanel.Name = "BackgroundsPanel";
            BackgroundsPanel.Padding = new Padding(10, 0, 0, 0);
            BackgroundsPanel.Size = new Size(900, 840);
            BackgroundsPanel.TabIndex = 8;
            // 
            // L_Sinterklaas
            // 
            L_Sinterklaas.AutoSize = true;
            L_Sinterklaas.Font = new Font("Times New Roman", 20F, FontStyle.Bold);
            L_Sinterklaas.Location = new Point(584, 730);
            L_Sinterklaas.Name = "L_Sinterklaas";
            L_Sinterklaas.Size = new Size(147, 31);
            L_Sinterklaas.TabIndex = 19;
            L_Sinterklaas.Text = "Sinterklaas";
            L_Sinterklaas.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // L_Christmas
            // 
            L_Christmas.AutoSize = true;
            L_Christmas.Font = new Font("Times New Roman", 20F, FontStyle.Bold);
            L_Christmas.Location = new Point(180, 730);
            L_Christmas.Name = "L_Christmas";
            L_Christmas.Size = new Size(136, 31);
            L_Christmas.TabIndex = 18;
            L_Christmas.Text = "Christmas";
            L_Christmas.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // L_Easter
            // 
            L_Easter.AutoSize = true;
            L_Easter.Font = new Font("Times New Roman", 20F, FontStyle.Bold);
            L_Easter.Location = new Point(607, 499);
            L_Easter.Name = "L_Easter";
            L_Easter.Size = new Size(90, 31);
            L_Easter.TabIndex = 17;
            L_Easter.Text = "Easter";
            L_Easter.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // L_Summer
            // 
            L_Summer.AutoSize = true;
            L_Summer.Font = new Font("Times New Roman", 20F, FontStyle.Bold);
            L_Summer.Location = new Point(192, 499);
            L_Summer.Name = "L_Summer";
            L_Summer.Size = new Size(112, 31);
            L_Summer.TabIndex = 16;
            L_Summer.Text = "Summer";
            L_Summer.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // L_Winter
            // 
            L_Winter.AutoSize = true;
            L_Winter.Font = new Font("Times New Roman", 20F, FontStyle.Bold);
            L_Winter.Location = new Point(603, 268);
            L_Winter.Name = "L_Winter";
            L_Winter.Size = new Size(98, 31);
            L_Winter.TabIndex = 11;
            L_Winter.Text = "Winter";
            L_Winter.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // L_Standard
            // 
            L_Standard.AutoSize = true;
            L_Standard.BackColor = Color.Silver;
            L_Standard.Font = new Font("Times New Roman", 20F, FontStyle.Bold);
            L_Standard.Location = new Point(187, 268);
            L_Standard.Name = "L_Standard";
            L_Standard.Size = new Size(123, 31);
            L_Standard.TabIndex = 10;
            L_Standard.Text = "Standard";
            L_Standard.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Sinterklaas
            // 
            Sinterklaas.BackColor = Color.Gray;
            Sinterklaas.Image = Properties.Resources.Sinterklaas;
            Sinterklaas.Location = new Point(497, 537);
            Sinterklaas.Name = "Sinterklaas";
            Sinterklaas.Padding = new Padding(10);
            Sinterklaas.Size = new Size(310, 185);
            Sinterklaas.TabIndex = 9;
            Sinterklaas.TabStop = false;
            Sinterklaas.Click += Click;
            Sinterklaas.MouseLeave += MouseLeave;
            Sinterklaas.MouseHover += MouseHover;
            // 
            // Christmas
            // 
            Christmas.BackColor = Color.Gray;
            Christmas.ErrorImage = null;
            Christmas.Image = Properties.Resources.Christmas;
            Christmas.Location = new Point(93, 537);
            Christmas.Name = "Christmas";
            Christmas.Padding = new Padding(10);
            Christmas.Size = new Size(310, 185);
            Christmas.TabIndex = 8;
            Christmas.TabStop = false;
            Christmas.Click += Click;
            Christmas.MouseLeave += MouseLeave;
            Christmas.MouseHover += MouseHover;
            // 
            // Easter
            // 
            Easter.BackColor = Color.Gray;
            Easter.Image = Properties.Resources.Easter;
            Easter.Location = new Point(497, 306);
            Easter.Name = "Easter";
            Easter.Padding = new Padding(10);
            Easter.Size = new Size(310, 185);
            Easter.TabIndex = 7;
            Easter.TabStop = false;
            Easter.Click += Click;
            Easter.MouseLeave += MouseLeave;
            Easter.MouseHover += MouseHover;
            // 
            // Summer
            // 
            Summer.BackColor = Color.Gray;
            Summer.Image = Properties.Resources.Summer;
            Summer.Location = new Point(93, 306);
            Summer.Name = "Summer";
            Summer.Padding = new Padding(10);
            Summer.Size = new Size(310, 185);
            Summer.TabIndex = 6;
            Summer.TabStop = false;
            Summer.Click += Click;
            Summer.MouseLeave += MouseLeave;
            Summer.MouseHover += MouseHover;
            // 
            // Winter
            // 
            Winter.BackColor = Color.Gray;
            Winter.Image = Properties.Resources.Winter;
            Winter.Location = new Point(497, 75);
            Winter.Name = "Winter";
            Winter.Padding = new Padding(10);
            Winter.Size = new Size(310, 185);
            Winter.TabIndex = 5;
            Winter.TabStop = false;
            Winter.Click += Click;
            Winter.MouseLeave += MouseLeave;
            Winter.MouseHover += MouseHover;
            // 
            // Standard
            // 
            Standard.BackColor = Color.Gray;
            Standard.Image = Properties.Resources.Standard;
            Standard.InitialImage = null;
            Standard.Location = new Point(93, 75);
            Standard.Name = "Standard";
            Standard.Padding = new Padding(10);
            Standard.Size = new Size(310, 185);
            Standard.TabIndex = 4;
            Standard.TabStop = false;
            Standard.Click += Click;
            Standard.MouseLeave += MouseLeave;
            Standard.MouseHover += MouseHover;
            // 
            // GoOptions
            // 
            GoOptions.BackColor = Color.Transparent;
            GoOptions.BackgroundImageLayout = ImageLayout.Center;
            GoOptions.FlatAppearance.BorderSize = 0;
            GoOptions.FlatStyle = FlatStyle.Flat;
            GoOptions.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            GoOptions.Image = Properties.Resources.button_main_menu_silver;
            GoOptions.Location = new Point(200, 759);
            GoOptions.Margin = new Padding(0);
            GoOptions.Name = "GoOptions";
            GoOptions.Size = new Size(500, 72);
            GoOptions.TabIndex = 3;
            GoOptions.Text = "Go to Options";
            GoOptions.UseVisualStyleBackColor = false;
            GoOptions.Click += Click;
            GoOptions.MouseLeave += MouseLeave;
            GoOptions.MouseHover += MouseHover;
            // 
            // BackgroundTitle
            // 
            BackgroundTitle.AutoSize = true;
            BackgroundTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            BackgroundTitle.Location = new Point(269, 5);
            BackgroundTitle.Name = "BackgroundTitle";
            BackgroundTitle.Size = new Size(363, 68);
            BackgroundTitle.TabIndex = 0;
            BackgroundTitle.Text = "Backgrounds";
            BackgroundTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Information_Panel
            // 
            Information_Panel.BackColor = Color.Silver;
            Information_Panel.BorderStyle = BorderStyle.FixedSingle;
            Information_Panel.Controls.Add(L_Information);
            Information_Panel.Controls.Add(Back3);
            Information_Panel.Controls.Add(Information_Title);
            Information_Panel.Location = new Point(175, 209);
            Information_Panel.Name = "Information_Panel";
            Information_Panel.Padding = new Padding(10, 0, 0, 0);
            Information_Panel.Size = new Size(900, 482);
            Information_Panel.TabIndex = 9;
            // 
            // L_Information
            // 
            L_Information.AutoSize = true;
            L_Information.BorderStyle = BorderStyle.FixedSingle;
            L_Information.Font = new Font("Times New Roman", 17F, FontStyle.Bold);
            L_Information.Location = new Point(115, 96);
            L_Information.Name = "L_Information";
            L_Information.Size = new Size(672, 252);
            L_Information.TabIndex = 4;
            L_Information.Text = resources.GetString("L_Information.Text");
            // 
            // Back3
            // 
            Back3.BackColor = Color.Transparent;
            Back3.BackgroundImageLayout = ImageLayout.Center;
            Back3.FlatAppearance.BorderSize = 0;
            Back3.FlatStyle = FlatStyle.Flat;
            Back3.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Back3.Image = Properties.Resources.button_main_menu_silver;
            Back3.Location = new Point(200, 370);
            Back3.Margin = new Padding(0);
            Back3.Name = "Back3";
            Back3.Size = new Size(500, 72);
            Back3.TabIndex = 3;
            Back3.Text = "Go to Main";
            Back3.UseVisualStyleBackColor = false;
            Back3.Click += Click;
            Back3.MouseLeave += MouseLeave;
            Back3.MouseHover += MouseHover;
            // 
            // Information_Title
            // 
            Information_Title.AutoSize = true;
            Information_Title.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            Information_Title.Location = new Point(281, 5);
            Information_Title.Name = "Information_Title";
            Information_Title.Size = new Size(338, 68);
            Information_Title.TabIndex = 0;
            Information_Title.Text = "Information";
            Information_Title.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Rules_Panel
            // 
            Rules_Panel.BackColor = Color.Silver;
            Rules_Panel.BorderStyle = BorderStyle.FixedSingle;
            Rules_Panel.Controls.Add(L_Rules);
            Rules_Panel.Controls.Add(Back4);
            Rules_Panel.Controls.Add(RulesTitle);
            Rules_Panel.Location = new Point(100, 58);
            Rules_Panel.Name = "Rules_Panel";
            Rules_Panel.Padding = new Padding(10, 0, 0, 0);
            Rules_Panel.Size = new Size(1050, 784);
            Rules_Panel.TabIndex = 10;
            // 
            // L_Rules
            // 
            L_Rules.AutoSize = true;
            L_Rules.BorderStyle = BorderStyle.FixedSingle;
            L_Rules.Font = new Font("Times New Roman", 17F, FontStyle.Bold);
            L_Rules.Location = new Point(28, 96);
            L_Rules.Name = "L_Rules";
            L_Rules.Size = new Size(995, 552);
            L_Rules.TabIndex = 4;
            L_Rules.Text = resources.GetString("L_Rules.Text");
            // 
            // Back4
            // 
            Back4.BackColor = Color.Transparent;
            Back4.BackgroundImageLayout = ImageLayout.Center;
            Back4.FlatAppearance.BorderSize = 0;
            Back4.FlatStyle = FlatStyle.Flat;
            Back4.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Back4.Image = Properties.Resources.button_main_menu_silver;
            Back4.Location = new Point(275, 672);
            Back4.Margin = new Padding(0);
            Back4.Name = "Back4";
            Back4.Size = new Size(500, 72);
            Back4.TabIndex = 3;
            Back4.Text = "Go to Main";
            Back4.UseVisualStyleBackColor = false;
            Back4.Click += Click;
            Back4.MouseLeave += MouseLeave;
            Back4.MouseHover += MouseHover;
            // 
            // RulesTitle
            // 
            RulesTitle.AutoSize = true;
            RulesTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            RulesTitle.Location = new Point(439, 5);
            RulesTitle.Name = "RulesTitle";
            RulesTitle.Size = new Size(172, 68);
            RulesTitle.TabIndex = 0;
            RulesTitle.Text = "Rules";
            RulesTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Single_Panel
            // 
            Single_Panel.BackColor = Color.Silver;
            Single_Panel.BorderStyle = BorderStyle.FixedSingle;
            Single_Panel.Controls.Add(SingleBughouse);
            Single_Panel.Controls.Add(SingleChess);
            Single_Panel.Controls.Add(GoGames1);
            Single_Panel.Controls.Add(Single_PanelTitle);
            Single_Panel.Location = new Point(175, 250);
            Single_Panel.Name = "Single_Panel";
            Single_Panel.Padding = new Padding(10, 0, 0, 0);
            Single_Panel.Size = new Size(900, 400);
            Single_Panel.TabIndex = 11;
            // 
            // SingleBughouse
            // 
            SingleBughouse.BackColor = Color.Transparent;
            SingleBughouse.BackgroundImageLayout = ImageLayout.Center;
            SingleBughouse.FlatAppearance.BorderSize = 0;
            SingleBughouse.FlatStyle = FlatStyle.Flat;
            SingleBughouse.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            SingleBughouse.Image = Properties.Resources.button_main_menu_silver;
            SingleBughouse.Location = new Point(200, 192);
            SingleBughouse.Margin = new Padding(0);
            SingleBughouse.Name = "SingleBughouse";
            SingleBughouse.Size = new Size(500, 72);
            SingleBughouse.TabIndex = 14;
            SingleBughouse.Text = "Bughouse Chess";
            SingleBughouse.UseVisualStyleBackColor = false;
            SingleBughouse.Click += Click;
            SingleBughouse.MouseLeave += MouseLeave;
            SingleBughouse.MouseHover += MouseHover;
            // 
            // SingleChess
            // 
            SingleChess.BackColor = Color.Transparent;
            SingleChess.BackgroundImageLayout = ImageLayout.Center;
            SingleChess.FlatAppearance.BorderSize = 0;
            SingleChess.FlatStyle = FlatStyle.Flat;
            SingleChess.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            SingleChess.Image = Properties.Resources.button_main_menu_silver;
            SingleChess.Location = new Point(200, 96);
            SingleChess.Margin = new Padding(0);
            SingleChess.Name = "SingleChess";
            SingleChess.Size = new Size(500, 72);
            SingleChess.TabIndex = 13;
            SingleChess.Tag = "";
            SingleChess.Text = "Regular Chess";
            SingleChess.UseVisualStyleBackColor = false;
            SingleChess.Click += Click;
            SingleChess.MouseLeave += MouseLeave;
            SingleChess.MouseHover += MouseHover;
            // 
            // GoGames1
            // 
            GoGames1.BackColor = Color.Transparent;
            GoGames1.BackgroundImageLayout = ImageLayout.Center;
            GoGames1.FlatAppearance.BorderSize = 0;
            GoGames1.FlatStyle = FlatStyle.Flat;
            GoGames1.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            GoGames1.Image = Properties.Resources.button_main_menu_silver;
            GoGames1.Location = new Point(200, 288);
            GoGames1.Margin = new Padding(0);
            GoGames1.Name = "GoGames1";
            GoGames1.Size = new Size(500, 72);
            GoGames1.TabIndex = 12;
            GoGames1.Text = "Go Back";
            GoGames1.UseVisualStyleBackColor = false;
            GoGames1.Click += Click;
            GoGames1.MouseLeave += MouseLeave;
            GoGames1.MouseHover += MouseHover;
            // 
            // Single_PanelTitle
            // 
            Single_PanelTitle.AutoSize = true;
            Single_PanelTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            Single_PanelTitle.Location = new Point(248, 5);
            Single_PanelTitle.Name = "Single_PanelTitle";
            Single_PanelTitle.Size = new Size(405, 68);
            Single_PanelTitle.TabIndex = 0;
            Single_PanelTitle.Text = "Game variants";
            Single_PanelTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Multiplayer_Panel
            // 
            Multiplayer_Panel.BackColor = Color.Silver;
            Multiplayer_Panel.BorderStyle = BorderStyle.FixedSingle;
            Multiplayer_Panel.Controls.Add(LAN);
            Multiplayer_Panel.Controls.Add(Local);
            Multiplayer_Panel.Controls.Add(GoGames2);
            Multiplayer_Panel.Controls.Add(Multiplayer_PanelTitle);
            Multiplayer_Panel.Location = new Point(175, 250);
            Multiplayer_Panel.Name = "Multiplayer_Panel";
            Multiplayer_Panel.Padding = new Padding(10, 0, 0, 0);
            Multiplayer_Panel.Size = new Size(900, 400);
            Multiplayer_Panel.TabIndex = 15;
            // 
            // LAN
            // 
            LAN.BackColor = Color.Transparent;
            LAN.BackgroundImageLayout = ImageLayout.Center;
            LAN.FlatAppearance.BorderSize = 0;
            LAN.FlatStyle = FlatStyle.Flat;
            LAN.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            LAN.Image = Properties.Resources.button_main_menu_silver;
            LAN.Location = new Point(200, 192);
            LAN.Margin = new Padding(0);
            LAN.Name = "LAN";
            LAN.Size = new Size(500, 72);
            LAN.TabIndex = 14;
            LAN.Text = "LAN";
            LAN.UseVisualStyleBackColor = false;
            LAN.Click += Click;
            LAN.MouseLeave += MouseLeave;
            LAN.MouseHover += MouseHover;
            // 
            // Local
            // 
            Local.BackColor = Color.Transparent;
            Local.BackgroundImageLayout = ImageLayout.Center;
            Local.FlatAppearance.BorderSize = 0;
            Local.FlatStyle = FlatStyle.Flat;
            Local.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Local.Image = Properties.Resources.button_main_menu_silver;
            Local.Location = new Point(200, 96);
            Local.Margin = new Padding(0);
            Local.Name = "Local";
            Local.Size = new Size(500, 72);
            Local.TabIndex = 13;
            Local.Tag = "";
            Local.Text = "Local Multiplayer";
            Local.UseVisualStyleBackColor = false;
            Local.Click += Click;
            Local.MouseLeave += MouseLeave;
            Local.MouseHover += MouseHover;
            // 
            // GoGames2
            // 
            GoGames2.BackColor = Color.Transparent;
            GoGames2.BackgroundImageLayout = ImageLayout.Center;
            GoGames2.FlatAppearance.BorderSize = 0;
            GoGames2.FlatStyle = FlatStyle.Flat;
            GoGames2.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            GoGames2.Image = Properties.Resources.button_main_menu_silver;
            GoGames2.Location = new Point(200, 288);
            GoGames2.Margin = new Padding(0);
            GoGames2.Name = "GoGames2";
            GoGames2.Size = new Size(500, 72);
            GoGames2.TabIndex = 12;
            GoGames2.Text = "Go Back";
            GoGames2.UseVisualStyleBackColor = false;
            GoGames2.Click += Click;
            GoGames2.MouseLeave += MouseLeave;
            GoGames2.MouseHover += MouseHover;
            // 
            // Multiplayer_PanelTitle
            // 
            Multiplayer_PanelTitle.AutoSize = true;
            Multiplayer_PanelTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            Multiplayer_PanelTitle.Location = new Point(282, 5);
            Multiplayer_PanelTitle.Name = "Multiplayer_PanelTitle";
            Multiplayer_PanelTitle.Size = new Size(337, 68);
            Multiplayer_PanelTitle.TabIndex = 0;
            Multiplayer_PanelTitle.Text = "Multiplayer";
            Multiplayer_PanelTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Local_Panel
            // 
            Local_Panel.BackColor = Color.Silver;
            Local_Panel.BorderStyle = BorderStyle.FixedSingle;
            Local_Panel.Controls.Add(LocalBughouse);
            Local_Panel.Controls.Add(LocalChess);
            Local_Panel.Controls.Add(LocalBack);
            Local_Panel.Controls.Add(Local_PanelTitle);
            Local_Panel.Location = new Point(175, 250);
            Local_Panel.Name = "Local_Panel";
            Local_Panel.Padding = new Padding(10, 0, 0, 0);
            Local_Panel.Size = new Size(900, 400);
            Local_Panel.TabIndex = 15;
            // 
            // LocalBughouse
            // 
            LocalBughouse.BackColor = Color.Transparent;
            LocalBughouse.BackgroundImageLayout = ImageLayout.Center;
            LocalBughouse.FlatAppearance.BorderSize = 0;
            LocalBughouse.FlatStyle = FlatStyle.Flat;
            LocalBughouse.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            LocalBughouse.Image = Properties.Resources.button_main_menu_silver;
            LocalBughouse.Location = new Point(200, 192);
            LocalBughouse.Margin = new Padding(0);
            LocalBughouse.Name = "LocalBughouse";
            LocalBughouse.Size = new Size(500, 72);
            LocalBughouse.TabIndex = 14;
            LocalBughouse.Text = "Bughouse Chess";
            LocalBughouse.UseVisualStyleBackColor = false;
            LocalBughouse.Click += Click;
            LocalBughouse.MouseLeave += MouseLeave;
            LocalBughouse.MouseHover += MouseHover;
            // 
            // LocalChess
            // 
            LocalChess.BackColor = Color.Transparent;
            LocalChess.BackgroundImageLayout = ImageLayout.Center;
            LocalChess.FlatAppearance.BorderSize = 0;
            LocalChess.FlatStyle = FlatStyle.Flat;
            LocalChess.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            LocalChess.Image = Properties.Resources.button_main_menu_silver;
            LocalChess.Location = new Point(200, 96);
            LocalChess.Margin = new Padding(0);
            LocalChess.Name = "LocalChess";
            LocalChess.Size = new Size(500, 72);
            LocalChess.TabIndex = 13;
            LocalChess.Tag = "";
            LocalChess.Text = "Regular Chess";
            LocalChess.UseVisualStyleBackColor = false;
            LocalChess.Click += Click;
            LocalChess.MouseLeave += MouseLeave;
            LocalChess.MouseHover += MouseHover;
            // 
            // LocalBack
            // 
            LocalBack.BackColor = Color.Transparent;
            LocalBack.BackgroundImageLayout = ImageLayout.Center;
            LocalBack.FlatAppearance.BorderSize = 0;
            LocalBack.FlatStyle = FlatStyle.Flat;
            LocalBack.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            LocalBack.Image = Properties.Resources.button_main_menu_silver;
            LocalBack.Location = new Point(200, 288);
            LocalBack.Margin = new Padding(0);
            LocalBack.Name = "LocalBack";
            LocalBack.Size = new Size(500, 72);
            LocalBack.TabIndex = 12;
            LocalBack.Text = "Go Back";
            LocalBack.UseVisualStyleBackColor = false;
            LocalBack.Click += Click;
            LocalBack.MouseLeave += MouseLeave;
            LocalBack.MouseHover += MouseHover;
            // 
            // Local_PanelTitle
            // 
            Local_PanelTitle.AutoSize = true;
            Local_PanelTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            Local_PanelTitle.Location = new Point(258, 5);
            Local_PanelTitle.Name = "Local_PanelTitle";
            Local_PanelTitle.Size = new Size(405, 68);
            Local_PanelTitle.TabIndex = 0;
            Local_PanelTitle.Text = "Game variants";
            Local_PanelTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // LocalTeam_Panel
            // 
            LocalTeam_Panel.BackColor = Color.Silver;
            LocalTeam_Panel.BorderStyle = BorderStyle.FixedSingle;
            LocalTeam_Panel.Controls.Add(Local_Versus);
            LocalTeam_Panel.Controls.Add(Local_Team);
            LocalTeam_Panel.Controls.Add(LocalTeamBack);
            LocalTeam_Panel.Controls.Add(LocalTeam_PanelTitle);
            LocalTeam_Panel.Location = new Point(175, 250);
            LocalTeam_Panel.Name = "LocalTeam_Panel";
            LocalTeam_Panel.Padding = new Padding(10, 0, 0, 0);
            LocalTeam_Panel.Size = new Size(900, 400);
            LocalTeam_Panel.TabIndex = 17;
            // 
            // Local_Versus
            // 
            Local_Versus.BackColor = Color.Transparent;
            Local_Versus.BackgroundImageLayout = ImageLayout.Center;
            Local_Versus.FlatAppearance.BorderSize = 0;
            Local_Versus.FlatStyle = FlatStyle.Flat;
            Local_Versus.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Local_Versus.Image = Properties.Resources.button_main_menu_silver;
            Local_Versus.Location = new Point(200, 192);
            Local_Versus.Margin = new Padding(0);
            Local_Versus.Name = "Local_Versus";
            Local_Versus.Size = new Size(500, 72);
            Local_Versus.TabIndex = 14;
            Local_Versus.Text = "Versus";
            Local_Versus.UseVisualStyleBackColor = false;
            Local_Versus.Click += Click;
            Local_Versus.MouseLeave += MouseLeave;
            Local_Versus.MouseHover += MouseHover;
            // 
            // Local_Team
            // 
            Local_Team.BackColor = Color.Transparent;
            Local_Team.BackgroundImageLayout = ImageLayout.Center;
            Local_Team.FlatAppearance.BorderSize = 0;
            Local_Team.FlatStyle = FlatStyle.Flat;
            Local_Team.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            Local_Team.Image = Properties.Resources.button_main_menu_silver;
            Local_Team.Location = new Point(200, 96);
            Local_Team.Margin = new Padding(0);
            Local_Team.Name = "Local_Team";
            Local_Team.Size = new Size(500, 72);
            Local_Team.TabIndex = 13;
            Local_Team.Tag = "";
            Local_Team.Text = "Co-op";
            Local_Team.UseVisualStyleBackColor = false;
            Local_Team.Click += Click;
            Local_Team.MouseLeave += MouseLeave;
            Local_Team.MouseHover += MouseHover;
            // 
            // LocalTeamBack
            // 
            LocalTeamBack.BackColor = Color.Transparent;
            LocalTeamBack.BackgroundImageLayout = ImageLayout.Center;
            LocalTeamBack.FlatAppearance.BorderSize = 0;
            LocalTeamBack.FlatStyle = FlatStyle.Flat;
            LocalTeamBack.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            LocalTeamBack.Image = Properties.Resources.button_main_menu_silver;
            LocalTeamBack.Location = new Point(200, 288);
            LocalTeamBack.Margin = new Padding(0);
            LocalTeamBack.Name = "LocalTeamBack";
            LocalTeamBack.Size = new Size(500, 72);
            LocalTeamBack.TabIndex = 12;
            LocalTeamBack.Text = "Go Back";
            LocalTeamBack.UseVisualStyleBackColor = false;
            LocalTeamBack.Click += Click;
            LocalTeamBack.MouseLeave += MouseLeave;
            LocalTeamBack.MouseHover += MouseHover;
            // 
            // LocalTeam_PanelTitle
            // 
            LocalTeam_PanelTitle.AutoSize = true;
            LocalTeam_PanelTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            LocalTeam_PanelTitle.Location = new Point(251, 5);
            LocalTeam_PanelTitle.Name = "LocalTeam_PanelTitle";
            LocalTeam_PanelTitle.Size = new Size(398, 68);
            LocalTeam_PanelTitle.TabIndex = 0;
            LocalTeam_PanelTitle.Text = "Co-op / Versus";
            LocalTeam_PanelTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // LocalPlayers_Panel
            // 
            LocalPlayers_Panel.BackColor = Color.Silver;
            LocalPlayers_Panel.BorderStyle = BorderStyle.FixedSingle;
            LocalPlayers_Panel.Controls.Add(FourPlayers);
            LocalPlayers_Panel.Controls.Add(TwoPlayers);
            LocalPlayers_Panel.Controls.Add(LocalPlayersBack);
            LocalPlayers_Panel.Controls.Add(LocalPlayers_PanelTitle);
            LocalPlayers_Panel.Location = new Point(175, 250);
            LocalPlayers_Panel.Name = "LocalPlayers_Panel";
            LocalPlayers_Panel.Padding = new Padding(10, 0, 0, 0);
            LocalPlayers_Panel.Size = new Size(900, 400);
            LocalPlayers_Panel.TabIndex = 19;
            // 
            // FourPlayers
            // 
            FourPlayers.BackColor = Color.Transparent;
            FourPlayers.BackgroundImageLayout = ImageLayout.Center;
            FourPlayers.FlatAppearance.BorderSize = 0;
            FourPlayers.FlatStyle = FlatStyle.Flat;
            FourPlayers.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            FourPlayers.Image = Properties.Resources.button_main_menu_silver;
            FourPlayers.Location = new Point(200, 192);
            FourPlayers.Margin = new Padding(0);
            FourPlayers.Name = "FourPlayers";
            FourPlayers.Size = new Size(500, 72);
            FourPlayers.TabIndex = 14;
            FourPlayers.Text = "4 players";
            FourPlayers.UseVisualStyleBackColor = false;
            FourPlayers.Click += Click;
            FourPlayers.MouseLeave += MouseLeave;
            FourPlayers.MouseHover += MouseHover;
            // 
            // TwoPlayers
            // 
            TwoPlayers.BackColor = Color.Transparent;
            TwoPlayers.BackgroundImageLayout = ImageLayout.Center;
            TwoPlayers.FlatAppearance.BorderSize = 0;
            TwoPlayers.FlatStyle = FlatStyle.Flat;
            TwoPlayers.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            TwoPlayers.Image = Properties.Resources.button_main_menu_silver;
            TwoPlayers.Location = new Point(200, 96);
            TwoPlayers.Margin = new Padding(0);
            TwoPlayers.Name = "TwoPlayers";
            TwoPlayers.Size = new Size(500, 72);
            TwoPlayers.TabIndex = 13;
            TwoPlayers.Tag = "";
            TwoPlayers.Text = "2 players";
            TwoPlayers.UseVisualStyleBackColor = false;
            TwoPlayers.Click += Click;
            TwoPlayers.MouseLeave += MouseLeave;
            TwoPlayers.MouseHover += MouseHover;
            // 
            // LocalPlayersBack
            // 
            LocalPlayersBack.BackColor = Color.Transparent;
            LocalPlayersBack.BackgroundImageLayout = ImageLayout.Center;
            LocalPlayersBack.FlatAppearance.BorderSize = 0;
            LocalPlayersBack.FlatStyle = FlatStyle.Flat;
            LocalPlayersBack.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            LocalPlayersBack.Image = Properties.Resources.button_main_menu_silver;
            LocalPlayersBack.Location = new Point(200, 288);
            LocalPlayersBack.Margin = new Padding(0);
            LocalPlayersBack.Name = "LocalPlayersBack";
            LocalPlayersBack.Size = new Size(500, 72);
            LocalPlayersBack.TabIndex = 12;
            LocalPlayersBack.Text = "Go Back";
            LocalPlayersBack.UseVisualStyleBackColor = false;
            LocalPlayersBack.Click += Click;
            LocalPlayersBack.MouseLeave += MouseLeave;
            LocalPlayersBack.MouseHover += MouseHover;
            // 
            // LocalPlayers_PanelTitle
            // 
            LocalPlayers_PanelTitle.AutoSize = true;
            LocalPlayers_PanelTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            LocalPlayers_PanelTitle.Location = new Point(340, 5);
            LocalPlayers_PanelTitle.Name = "LocalPlayers_PanelTitle";
            LocalPlayers_PanelTitle.Size = new Size(221, 68);
            LocalPlayers_PanelTitle.TabIndex = 0;
            LocalPlayers_PanelTitle.Text = "Players";
            LocalPlayers_PanelTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // Host_Panel
            // 
            Host_Panel.BackColor = Color.Silver;
            Host_Panel.BorderStyle = BorderStyle.FixedSingle;
            Host_Panel.Controls.Add(NoHostButton);
            Host_Panel.Controls.Add(HostButton);
            Host_Panel.Controls.Add(HostBack);
            Host_Panel.Controls.Add(Host_PanelTitle);
            Host_Panel.Location = new Point(175, 250);
            Host_Panel.Name = "Host_Panel";
            Host_Panel.Padding = new Padding(10, 0, 0, 0);
            Host_Panel.Size = new Size(900, 400);
            Host_Panel.TabIndex = 20;
            // 
            // NoHostButton
            // 
            NoHostButton.BackColor = Color.Transparent;
            NoHostButton.BackgroundImageLayout = ImageLayout.Center;
            NoHostButton.FlatAppearance.BorderSize = 0;
            NoHostButton.FlatStyle = FlatStyle.Flat;
            NoHostButton.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            NoHostButton.Image = Properties.Resources.button_main_menu_silver;
            NoHostButton.Location = new Point(200, 192);
            NoHostButton.Margin = new Padding(0);
            NoHostButton.Name = "NoHostButton";
            NoHostButton.Size = new Size(500, 72);
            NoHostButton.TabIndex = 14;
            NoHostButton.Text = "I am not Host";
            NoHostButton.UseVisualStyleBackColor = false;
            NoHostButton.Click += Click;
            NoHostButton.MouseLeave += MouseLeave;
            NoHostButton.MouseHover += MouseHover;
            // 
            // HostButton
            // 
            HostButton.BackColor = Color.Transparent;
            HostButton.BackgroundImageLayout = ImageLayout.Center;
            HostButton.FlatAppearance.BorderSize = 0;
            HostButton.FlatStyle = FlatStyle.Flat;
            HostButton.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            HostButton.Image = Properties.Resources.button_main_menu_silver;
            HostButton.Location = new Point(200, 96);
            HostButton.Margin = new Padding(0);
            HostButton.Name = "HostButton";
            HostButton.Size = new Size(500, 72);
            HostButton.TabIndex = 13;
            HostButton.Tag = "";
            HostButton.Text = "I am Host";
            HostButton.UseVisualStyleBackColor = false;
            HostButton.Click += Click;
            HostButton.MouseLeave += MouseLeave;
            HostButton.MouseHover += MouseHover;
            // 
            // HostBack
            // 
            HostBack.BackColor = Color.Transparent;
            HostBack.BackgroundImageLayout = ImageLayout.Center;
            HostBack.FlatAppearance.BorderSize = 0;
            HostBack.FlatStyle = FlatStyle.Flat;
            HostBack.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            HostBack.Image = Properties.Resources.button_main_menu_silver;
            HostBack.Location = new Point(200, 288);
            HostBack.Margin = new Padding(0);
            HostBack.Name = "HostBack";
            HostBack.Size = new Size(500, 72);
            HostBack.TabIndex = 12;
            HostBack.Text = "Go Back";
            HostBack.UseVisualStyleBackColor = false;
            HostBack.Click += Click;
            HostBack.MouseLeave += MouseLeave;
            HostBack.MouseHover += MouseHover;
            // 
            // Host_PanelTitle
            // 
            Host_PanelTitle.AutoSize = true;
            Host_PanelTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            Host_PanelTitle.Location = new Point(361, 5);
            Host_PanelTitle.Name = "Host_PanelTitle";
            Host_PanelTitle.Size = new Size(178, 68);
            Host_PanelTitle.TabIndex = 0;
            Host_PanelTitle.Text = "Host?";
            Host_PanelTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // IP_Panel
            // 
            IP_Panel.BackColor = Color.Silver;
            IP_Panel.BorderStyle = BorderStyle.FixedSingle;
            IP_Panel.Controls.Add(GoIP);
            IP_Panel.Controls.Add(IPadressFill);
            IP_Panel.Controls.Add(IPBack);
            IP_Panel.Controls.Add(IP_PanelTitle);
            IP_Panel.Location = new Point(175, 250);
            IP_Panel.Name = "IP_Panel";
            IP_Panel.Padding = new Padding(10, 0, 0, 0);
            IP_Panel.Size = new Size(900, 400);
            IP_Panel.TabIndex = 21;
            // 
            // GoIP
            // 
            GoIP.BackColor = Color.Transparent;
            GoIP.BackgroundImageLayout = ImageLayout.Center;
            GoIP.FlatAppearance.BorderSize = 0;
            GoIP.FlatStyle = FlatStyle.Flat;
            GoIP.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            GoIP.Image = Properties.Resources.button_main_menu_silver;
            GoIP.Location = new Point(199, 192);
            GoIP.Margin = new Padding(0);
            GoIP.Name = "GoIP";
            GoIP.Size = new Size(500, 72);
            GoIP.TabIndex = 14;
            GoIP.Text = "Go!";
            GoIP.UseVisualStyleBackColor = false;
            GoIP.Click += Click;
            GoIP.MouseLeave += MouseLeave;
            GoIP.MouseHover += MouseHover;
            // 
            // IPadressFill
            // 
            IPadressFill.Font = new Font("Times New Roman", 18F, FontStyle.Bold);
            IPadressFill.Location = new Point(200, 133);
            IPadressFill.Name = "IPadressFill";
            IPadressFill.Size = new Size(500, 35);
            IPadressFill.TabIndex = 13;
            // 
            // IPBack
            // 
            IPBack.BackColor = Color.Transparent;
            IPBack.BackgroundImageLayout = ImageLayout.Center;
            IPBack.FlatAppearance.BorderSize = 0;
            IPBack.FlatStyle = FlatStyle.Flat;
            IPBack.Font = new Font("Times New Roman", 24F, FontStyle.Bold);
            IPBack.Image = Properties.Resources.button_main_menu_silver;
            IPBack.Location = new Point(200, 288);
            IPBack.Margin = new Padding(0);
            IPBack.Name = "IPBack";
            IPBack.Size = new Size(500, 72);
            IPBack.TabIndex = 12;
            IPBack.Text = "Go Back";
            IPBack.UseVisualStyleBackColor = false;
            IPBack.Click += Click;
            IPBack.MouseLeave += MouseLeave;
            IPBack.MouseHover += MouseHover;
            // 
            // IP_PanelTitle
            // 
            IP_PanelTitle.AutoSize = true;
            IP_PanelTitle.Font = new Font("Times New Roman", 45F, FontStyle.Bold);
            IP_PanelTitle.Location = new Point(168, 5);
            IP_PanelTitle.Name = "IP_PanelTitle";
            IP_PanelTitle.Size = new Size(564, 68);
            IP_PanelTitle.TabIndex = 0;
            IP_PanelTitle.Text = "IP adress from Host?";
            IP_PanelTitle.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // MainMenu
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ControlDark;
            ClientSize = new Size(1234, 861);
            Controls.Add(IP_Panel);
            Controls.Add(Host_Panel);
            Controls.Add(LocalPlayers_Panel);
            Controls.Add(LocalTeam_Panel);
            Controls.Add(Options_Panel);
            Controls.Add(Multiplayer_Panel);
            Controls.Add(Local_Panel);
            Controls.Add(Single_Panel);
            Controls.Add(Rules_Panel);
            Controls.Add(Information_Panel);
            Controls.Add(Games_Panel);
            Controls.Add(BackgroundsPanel);
            Controls.Add(Main_Panel);
            Font = new Font("Segoe UI", 9F, FontStyle.Bold);
            MaximizeBox = false;
            Name = "MainMenu";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "MainMenu";
            Load += MainMenu_Load;
            Main_Panel.ResumeLayout(false);
            Main_Panel.PerformLayout();
            Options_Panel.ResumeLayout(false);
            Options_Panel.PerformLayout();
            Games_Panel.ResumeLayout(false);
            Games_Panel.PerformLayout();
            BackgroundsPanel.ResumeLayout(false);
            BackgroundsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)Sinterklaas).EndInit();
            ((System.ComponentModel.ISupportInitialize)Christmas).EndInit();
            ((System.ComponentModel.ISupportInitialize)Easter).EndInit();
            ((System.ComponentModel.ISupportInitialize)Summer).EndInit();
            ((System.ComponentModel.ISupportInitialize)Winter).EndInit();
            ((System.ComponentModel.ISupportInitialize)Standard).EndInit();
            Information_Panel.ResumeLayout(false);
            Information_Panel.PerformLayout();
            Rules_Panel.ResumeLayout(false);
            Rules_Panel.PerformLayout();
            Single_Panel.ResumeLayout(false);
            Single_Panel.PerformLayout();
            Multiplayer_Panel.ResumeLayout(false);
            Multiplayer_Panel.PerformLayout();
            Local_Panel.ResumeLayout(false);
            Local_Panel.PerformLayout();
            LocalTeam_Panel.ResumeLayout(false);
            LocalTeam_Panel.PerformLayout();
            LocalPlayers_Panel.ResumeLayout(false);
            LocalPlayers_Panel.PerformLayout();
            Host_Panel.ResumeLayout(false);
            Host_Panel.PerformLayout();
            IP_Panel.ResumeLayout(false);
            IP_Panel.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Panel Main_Panel;
        private Label Title;
        private Button Start;
        private Button Quit;
        private Button Options;
        private Panel Options_Panel;
        private Button Back1;
        private Button BackGround;
        private Label OptionsTitle;
        private Panel BackgroundsPanel;
        private Button GoOptions;
        private Label BackgroundTitle;
        private PictureBox Sinterklaas;
        private PictureBox Christmas;
        private PictureBox Easter;
        private PictureBox Summer;
        private PictureBox Winter;
        private PictureBox Standard;
        private Label L_Summer;
        private Label L_Winter;
        private Label L_Standard;
        private Label L_Sinterklaas;
        private Label L_Christmas;
        private Label L_Easter;
        private Panel Games_Panel;
        private Button Back2;
        private Button Singleplayer;
        private Label Games;
        private Button Multiplayer;
        private Button Information;
        private Button Rules;
        private Panel Information_Panel;
        private Label L_Information;
        private Button Back3;
        private Label Information_Title;
        private Panel Rules_Panel;
        private Label L_Rules;
        private Button Back4;
        private Label RulesTitle;
        private Panel Single_Panel;
        private Button GoGames1;
        private Label Single_PanelTitle;
        private Button SingleBughouse;
        private Button SingleChess;
        private Panel Multiplayer_Panel;
        private Button LAN;
        private Button Local;
        private Button GoGames2;
        private Label Multiplayer_PanelTitle;
        private Panel Local_Panel;
        private Button LocalBughouse;
        private Button LocalChess;
        private Button LocalBack;
        private Label Local_PanelTitle;
        private Panel LocalTeam_Panel;
        private Button Local_Versus;
        private Button Local_Team;
        private Button LocalTeamBack;
        private Label LocalTeam_PanelTitle;
        private Panel LocalPlayers_Panel;
        private Button FourPlayers;
        private Button TwoPlayers;
        private Button LocalPlayersBack;
        private Label LocalPlayers_PanelTitle;
        private Label Sounds;
        private Panel Host_Panel;
        private Button NoHostButton;
        private Button HostButton;
        private Button HostBack;
        private Label Host_PanelTitle;
        private Panel IP_Panel;
        private Button IPBack;
        private Label IP_PanelTitle;
        private Button GoIP;
        private TextBox IPadressFill;
        private Button SoundOff;
        private Button SoundOn;
    }
}