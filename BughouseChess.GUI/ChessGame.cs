using BughouseChess.Core;
using System.Drawing;
using System.ComponentModel;
using System.Net.Sockets;
using System.Windows.Forms.ComponentModel.Com2Interop;

namespace BughouseChess.GUI;
public class ChessGame : Form
{
    public int chessboardSize = 480;
    public int buttonHeight = 50;
    public int buttonWidth = 240;
    internal int minute = 10;
    internal int second = 0;

    public Chessboard c1;
    public ChessTimer chesstimerWhiteBoard1, chesstimerBlackBoard1;
    public ChessButton resignWhite1, resignBlack1;
    public ChessList blacklist1, whitelist1;
    public Label chessboard1;
    public Label timerWhite1, timerBlack1;
    public Label button1, button2, chesslistwhite1, chesslistblack1;
    public Label turnDotlabel1;

    /// <summary>
    /// Creates the form for the chessgame. Including buttons, timers and background. 
    /// </summary>
    public ChessGame()
    {
        InitializeComponent();
    }
    private void InitializeComponent()
    {
        DoubleBuffered = true;
        // Size and text for the form.
        this.AutoScaleMode = AutoScaleMode.Font;
        this.ClientSize = new Size(Screen.FromControl(this).Bounds.Width, Screen.FromControl(this).Bounds.Height);
        this.Text = "BughouseChess";

        this.StartPosition = FormStartPosition.CenterScreen;

        // Labels which contain our own chessboards.
        this.chessboard1 = new Label();

        // Constructs an own designed chessboard.
        this.c1 = new Chessboard(chessboard1, chessboardSize, new Point(this.ClientSize.Width / 2 - chessboardSize / 2 - chessboardSize / 16, this.ClientSize.Height / 2 - chessboardSize / 2 - chessboardSize / 16), false);

        // Labels which function as containers for our own designed Graphical User Interfaces.
        this.timerWhite1 = new Label();
        this.timerBlack1 = new Label();
        this.button1 = new Label();
        this.button2 = new Label();
        this.chesslistwhite1 = new Label();
        this.chesslistblack1 = new Label();

        this.turnDotlabel1 = new Label();

        // We do not want the labels which contain the GUI to be visible.
        button1.BackColor = Color.Transparent;
        button2.BackColor = Color.Transparent;
        timerWhite1.BackColor = Color.Transparent;
        timerBlack1.BackColor = Color.Transparent;
        chesslistwhite1.BackColor = Color.Transparent;
        chesslistblack1.BackColor = Color.Transparent;
        turnDotlabel1.BackColor = Color.Transparent;

        // We use the Windows Forms Timer class for the functionality of the timer.
        System.Windows.Forms.Timer timerWhiteBoard1 = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer timerBlackBoard1 = new System.Windows.Forms.Timer();

        // Default settings for the timers in the beginning.
        timerWhiteBoard1.Interval = 1000;
        timerBlackBoard1.Interval = 1000;

        // Sets the numbers the timer must have in the beginning.
        timerWhiteBoard1.Tag = $"{minute}:0{second}";
        timerBlackBoard1.Tag = $"{minute}:0{second}";

        // Our own customized Graphical User interfaces.
        this.resignWhite1 = new ChessButton(new Point(c1.location.X, c1.location.Y + chessboardSize + chessboardSize / 8 + buttonHeight * 2), new Size(buttonWidth, buttonHeight), "Resign", button1);
        this.resignBlack1 = new ChessButton(new Point(c1.location.X, c1.location.Y - buttonHeight * 3), new Size(buttonWidth, buttonHeight), "Resign", button2);
        this.chesstimerWhiteBoard1 = new ChessTimer(new Point(c1.location.X + chessboardSize + chessboardSize / 8 - buttonWidth, c1.location.Y + chessboardSize + chessboardSize / 8 + buttonHeight * 2), new Size(buttonWidth, buttonHeight), $"{timerWhiteBoard1.Tag}", timerWhite1, timerWhiteBoard1);
        this.chesstimerBlackBoard1 = new ChessTimer(new Point(c1.location.X + chessboardSize + chessboardSize / 8 - buttonWidth, c1.location.Y - buttonHeight * 3), new Size(buttonWidth, buttonHeight), $"{timerBlackBoard1.Tag}", timerBlack1, timerBlackBoard1);
        this.blacklist1 = new ChessList(new Point(c1.location.X + chessboardSize / 16, c1.location.Y - 80), new Size(chessboardSize, 60), c1.piecelistBlack.piecelist, chesslistblack1);
        this.whitelist1 = new ChessList(new Point(c1.location.X + chessboardSize / 16, c1.location.Y + chessboardSize + chessboardSize / 16 + 50), new Size(chessboardSize, 60), c1.piecelistWhite.piecelist, chesslistwhite1);
        
        // Assign important variables.
        c1.timer1 = chesstimerWhiteBoard1;
        c1.timer2 = chesstimerBlackBoard1;
        c1.resignwhite = resignWhite1;
        c1.resignblack = resignBlack1;
        c1.piecelistWhite = whitelist1;
        c1.piecelistBlack = blacklist1;

        c1.turnDot = turnDotlabel1;

        blacklist1.side = Side.Black;
        whitelist1.side = Side.White;

        c1.active = true;

        c1.firstSide = Side.White;


        c1.form = this;
        c1.timer1.form = this;
        c1.timer2.form = this;

        // Call the CreateChessboard method to build the chessboards.
        c1.CreateChessboard();

        // Add our labels which contain GUI to the form.
        this.Controls.Add(chessboard1);
        this.Controls.Add(button1);
        this.Controls.Add(button2);
        this.Controls.Add(timerWhite1);
        this.Controls.Add(timerBlack1);
        this.Controls.Add(chesslistwhite1);
        this.Controls.Add(chesslistblack1);
        this.Controls.Add(turnDotlabel1);


        // Event handlers to make sure the timer starts, resign functionality and pieces can move.
        c1.chessboard.MouseClick += c1.Move;
        c1.resignwhite.label.MouseClick += c1.Resign;
        c1.resignblack.label.MouseClick += c1.Resign;
        c1.piecelistBlack.label.MouseClick += (o, mea) => c1.MovefromList(o, mea, c1.piecelistBlack);
        c1.piecelistWhite.label.MouseClick += (o, mea) => c1.MovefromList(o, mea, c1.piecelistWhite);
    }
}