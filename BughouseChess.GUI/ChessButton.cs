﻿using BughouseChess.Core;

namespace BughouseChess.GUI
{
    public class ChessButton : ChessTimer
    {
        public ChessButton()
        {
            this.text = "Default";
            label.MouseDown += ColorChange;
            label.MouseUp += ColorChange;
            label.MouseHover += ButtonHover;
            label.MouseLeave += ButtonLeave;
        }

        public ChessButton(Point p, Size s, string text, Label label)
        {
            this.p = p;
            this.s = s;
            this.text = text;
            this.label = label;
            this.bitmap = new Bitmap(s.Width, s.Height);
            label.Size = new Size(s.Width, s.Height);
            label.Location = p;
            label.Image = bitmap;
            label.Paint += Draw;
            label.MouseDown += ColorChange;
            label.MouseUp += ColorChange;
            label.MouseHover += ButtonHover;
            label.MouseLeave += ButtonLeave;
        }

        public override void Draw(object o, PaintEventArgs pea)
        {
            Graphics gr = pea.Graphics;
            base.Draw(o, pea);
            Font font = new Font("Times New Roman", 24, FontStyle.Bold);
            SizeF stringsize = gr.MeasureString(text, font, s.Width);
            gr.DrawString(text, font, Brushes.Black, new Point((s.Width - (int)stringsize.Width) / 2, (s.Height - (int)stringsize.Height) / 2));
        }

        /// <summary>
        /// Makes sure the backcolor of the button changes briefly when it is clicked on.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="mea"></param>
        void ColorChange(object o, MouseEventArgs mea)
        {
            if (change == 1)
            {
                brush = Brushes.DarkGray;
                change *= -1;
                label.Cursor = Cursors.Hand;
            }
            else if (change == -1)
            {
                brush = Brushes.LightGray;
                change *= -1;
                label.Cursor = Cursors.Default;
            }
            label.Invalidate();
        }
        void ButtonHover(object o, EventArgs mea)
        {
            brush = Brushes.DarkGray;
            label.Cursor = Cursors.Hand;
            label.Invalidate();
        }
        void ButtonLeave(object o, EventArgs mea) 
        { 
            brush = Brushes.LightGray;
            label.Cursor = Cursors.Default;
            label.Invalidate();
        }
    }
}
