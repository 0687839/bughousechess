﻿using System.Drawing;
using System.ComponentModel;
using System.Net.Sockets;
using BughouseChess.Core;
using System.Security.Cryptography.Xml;
using System;

namespace BughouseChess.GUI;

public class ChessGameOnlineHost : ChessGame
{
    public ChessGameOnlineHost()
    {
        c1.online = true;
        c1.Host = true;
        c1.MessageReceiver.DoWork += c1.MessageReceiver_DoWork;
        CheckForIllegalCrossThreadCalls = false;
        c1.active = true;
        c1.server = new TcpListener(System.Net.IPAddress.Any, 5732);
        c1.server.Start();
        c1.sock = c1.server.AcceptSocket();
    }
}